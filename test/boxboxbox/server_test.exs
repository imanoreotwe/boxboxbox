defmodule Boxboxbox.ServerTest do
  use Boxboxbox.DataCase

  alias Boxboxbox.Server

  describe "formation_lap_types" do
    alias Boxboxbox.Server.FormationLapType

    import Boxboxbox.ServerFixtures

    @invalid_attrs %{name: nil, value: nil, description: nil}

    test "list_formation_lap_types/0 returns all formation_lap_types" do
      formation_lap_type = formation_lap_type_fixture()
      assert Server.list_formation_lap_types() == [formation_lap_type]
    end

    test "get_formation_lap_type!/1 returns the formation_lap_type with given id" do
      formation_lap_type = formation_lap_type_fixture()
      assert Server.get_formation_lap_type!(formation_lap_type.id) == formation_lap_type
    end

    test "create_formation_lap_type/1 with valid data creates a formation_lap_type" do
      valid_attrs = %{name: "some name", value: 42, description: "some description"}

      assert {:ok, %FormationLapType{} = formation_lap_type} = Server.create_formation_lap_type(valid_attrs)
      assert formation_lap_type.name == "some name"
      assert formation_lap_type.value == 42
      assert formation_lap_type.description == "some description"
    end

    test "create_formation_lap_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Server.create_formation_lap_type(@invalid_attrs)
    end

    test "update_formation_lap_type/2 with valid data updates the formation_lap_type" do
      formation_lap_type = formation_lap_type_fixture()
      update_attrs = %{name: "some updated name", value: 43, description: "some updated description"}

      assert {:ok, %FormationLapType{} = formation_lap_type} = Server.update_formation_lap_type(formation_lap_type, update_attrs)
      assert formation_lap_type.name == "some updated name"
      assert formation_lap_type.value == 43
      assert formation_lap_type.description == "some updated description"
    end

    test "update_formation_lap_type/2 with invalid data returns error changeset" do
      formation_lap_type = formation_lap_type_fixture()
      assert {:error, %Ecto.Changeset{}} = Server.update_formation_lap_type(formation_lap_type, @invalid_attrs)
      assert formation_lap_type == Server.get_formation_lap_type!(formation_lap_type.id)
    end

    test "delete_formation_lap_type/1 deletes the formation_lap_type" do
      formation_lap_type = formation_lap_type_fixture()
      assert {:ok, %FormationLapType{}} = Server.delete_formation_lap_type(formation_lap_type)
      assert_raise Ecto.NoResultsError, fn -> Server.get_formation_lap_type!(formation_lap_type.id) end
    end

    test "change_formation_lap_type/1 returns a formation_lap_type changeset" do
      formation_lap_type = formation_lap_type_fixture()
      assert %Ecto.Changeset{} = Server.change_formation_lap_type(formation_lap_type)
    end
  end
end
