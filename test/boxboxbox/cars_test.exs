defmodule Boxboxbox.CarsTest do
  use Boxboxbox.DataCase

  alias Boxboxbox.Cars

  describe "car_groups" do
    alias Boxboxbox.Cars.CarGroup

    import Boxboxbox.CarsFixtures

    @invalid_attrs %{name: nil, value: nil}

    test "list_car_groups/0 returns all car_groups" do
      car_group = car_group_fixture()
      assert Cars.list_car_groups() == [car_group]
    end

    test "get_car_group!/1 returns the car_group with given id" do
      car_group = car_group_fixture()
      assert Cars.get_car_group!(car_group.id) == car_group
    end

    test "create_car_group/1 with valid data creates a car_group" do
      valid_attrs = %{name: "some name", value: "some value"}

      assert {:ok, %CarGroup{} = car_group} = Cars.create_car_group(valid_attrs)
      assert car_group.name == "some name"
      assert car_group.value == "some value"
    end

    test "create_car_group/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Cars.create_car_group(@invalid_attrs)
    end

    test "update_car_group/2 with valid data updates the car_group" do
      car_group = car_group_fixture()
      update_attrs = %{name: "some updated name", value: "some updated value"}

      assert {:ok, %CarGroup{} = car_group} = Cars.update_car_group(car_group, update_attrs)
      assert car_group.name == "some updated name"
      assert car_group.value == "some updated value"
    end

    test "update_car_group/2 with invalid data returns error changeset" do
      car_group = car_group_fixture()
      assert {:error, %Ecto.Changeset{}} = Cars.update_car_group(car_group, @invalid_attrs)
      assert car_group == Cars.get_car_group!(car_group.id)
    end

    test "delete_car_group/1 deletes the car_group" do
      car_group = car_group_fixture()
      assert {:ok, %CarGroup{}} = Cars.delete_car_group(car_group)
      assert_raise Ecto.NoResultsError, fn -> Cars.get_car_group!(car_group.id) end
    end

    test "change_car_group/1 returns a car_group changeset" do
      car_group = car_group_fixture()
      assert %Ecto.Changeset{} = Cars.change_car_group(car_group)
    end
  end
end
