defmodule Boxboxbox.ServersTest do
  use Boxboxbox.DataCase

  alias Boxboxbox.Servers

  describe "configuration_jsons" do
    alias Boxboxbox.Servers.ConfigurationJSON

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{udpPort: nil, tcpPort: nil, registerToLobby: nil, maxConnections: nil, lanDiscovery: nil, publicIP: nil}

    test "list_configuration_jsons/0 returns all configuration_jsons" do
      configuration_json = configuration_json_fixture()
      assert Servers.list_configuration_jsons() == [configuration_json]
    end

    test "get_configuration_json!/1 returns the configuration_json with given id" do
      configuration_json = configuration_json_fixture()
      assert Servers.get_configuration_json!(configuration_json.id) == configuration_json
    end

    test "create_configuration_json/1 with valid data creates a configuration_json" do
      valid_attrs = %{udpPort: 42, tcpPort: 42, registerToLobby: true, maxConnections: 42, lanDiscovery: true, publicIP: "some publicIP"}

      assert {:ok, %ConfigurationJSON{} = configuration_json} = Servers.create_configuration_json(valid_attrs)
      assert configuration_json.udpPort == 42
      assert configuration_json.tcpPort == 42
      assert configuration_json.registerToLobby == true
      assert configuration_json.maxConnections == 42
      assert configuration_json.lanDiscovery == true
      assert configuration_json.publicIP == "some publicIP"
    end

    test "create_configuration_json/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_configuration_json(@invalid_attrs)
    end

    test "update_configuration_json/2 with valid data updates the configuration_json" do
      configuration_json = configuration_json_fixture()
      update_attrs = %{udpPort: 43, tcpPort: 43, registerToLobby: false, maxConnections: 43, lanDiscovery: false, publicIP: "some updated publicIP"}

      assert {:ok, %ConfigurationJSON{} = configuration_json} = Servers.update_configuration_json(configuration_json, update_attrs)
      assert configuration_json.udpPort == 43
      assert configuration_json.tcpPort == 43
      assert configuration_json.registerToLobby == false
      assert configuration_json.maxConnections == 43
      assert configuration_json.lanDiscovery == false
      assert configuration_json.publicIP == "some updated publicIP"
    end

    test "update_configuration_json/2 with invalid data returns error changeset" do
      configuration_json = configuration_json_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_configuration_json(configuration_json, @invalid_attrs)
      assert configuration_json == Servers.get_configuration_json!(configuration_json.id)
    end

    test "delete_configuration_json/1 deletes the configuration_json" do
      configuration_json = configuration_json_fixture()
      assert {:ok, %ConfigurationJSON{}} = Servers.delete_configuration_json(configuration_json)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_configuration_json!(configuration_json.id) end
    end

    test "change_configuration_json/1 returns a configuration_json changeset" do
      configuration_json = configuration_json_fixture()
      assert %Ecto.Changeset{} = Servers.change_configuration_json(configuration_json)
    end
  end

  describe "servers" do
    alias Boxboxbox.Servers.Server

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{}

    test "list_servers/0 returns all servers" do
      server = server_fixture()
      assert Servers.list_servers() == [server]
    end

    test "get_server!/1 returns the server with given id" do
      server = server_fixture()
      assert Servers.get_server!(server.id) == server
    end

    test "create_server/1 with valid data creates a server" do
      valid_attrs = %{}

      assert {:ok, %Server{} = server} = Servers.create_server(valid_attrs)
    end

    test "create_server/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_server(@invalid_attrs)
    end

    test "update_server/2 with valid data updates the server" do
      server = server_fixture()
      update_attrs = %{}

      assert {:ok, %Server{} = server} = Servers.update_server(server, update_attrs)
    end

    test "update_server/2 with invalid data returns error changeset" do
      server = server_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_server(server, @invalid_attrs)
      assert server == Servers.get_server!(server.id)
    end

    test "delete_server/1 deletes the server" do
      server = server_fixture()
      assert {:ok, %Server{}} = Servers.delete_server(server)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_server!(server.id) end
    end

    test "change_server/1 returns a server changeset" do
      server = server_fixture()
      assert %Ecto.Changeset{} = Servers.change_server(server)
    end
  end

  describe "setting_jsons" do
    alias Boxboxbox.Servers.SettingJSON

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{password: nil, serverName: nil, adminPassword: nil, trackMedalsRequirement: nil, safetyRatingRequirement: nil, racecraftRatingRequirement: nil, spectatorPassword: nil, maxCarSlots: nil, dumpLeaderboards: nil, isRaceLocked: nil, randomizeTrackWhenEmpty: nil, centralEntryListPath: nil, allowAutoDQ: nil, shortFormationLap: nil, dumpEntryList: nil, ignorePrematureDisconnects: nil}

    test "list_setting_jsons/0 returns all setting_jsons" do
      setting_json = setting_json_fixture()
      assert Servers.list_setting_jsons() == [setting_json]
    end

    test "get_setting_json!/1 returns the setting_json with given id" do
      setting_json = setting_json_fixture()
      assert Servers.get_setting_json!(setting_json.id) == setting_json
    end

    test "create_setting_json/1 with valid data creates a setting_json" do
      valid_attrs = %{password: "some password", serverName: "some serverName", adminPassword: "some adminPassword", trackMedalsRequirement: 42, safetyRatingRequirement: 42, racecraftRatingRequirement: 42, spectatorPassword: "some spectatorPassword", maxCarSlots: 42, dumpLeaderboards: true, isRaceLocked: true, randomizeTrackWhenEmpty: true, centralEntryListPath: "some centralEntryListPath", allowAutoDQ: true, shortFormationLap: true, dumpEntryList: true, ignorePrematureDisconnects: true}

      assert {:ok, %SettingJSON{} = setting_json} = Servers.create_setting_json(valid_attrs)
      assert setting_json.password == "some password"
      assert setting_json.serverName == "some serverName"
      assert setting_json.adminPassword == "some adminPassword"
      assert setting_json.trackMedalsRequirement == 42
      assert setting_json.safetyRatingRequirement == 42
      assert setting_json.racecraftRatingRequirement == 42
      assert setting_json.spectatorPassword == "some spectatorPassword"
      assert setting_json.maxCarSlots == 42
      assert setting_json.dumpLeaderboards == true
      assert setting_json.isRaceLocked == true
      assert setting_json.randomizeTrackWhenEmpty == true
      assert setting_json.centralEntryListPath == "some centralEntryListPath"
      assert setting_json.allowAutoDQ == true
      assert setting_json.shortFormationLap == true
      assert setting_json.dumpEntryList == true
      assert setting_json.ignorePrematureDisconnects == true
    end

    test "create_setting_json/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_setting_json(@invalid_attrs)
    end

    test "update_setting_json/2 with valid data updates the setting_json" do
      setting_json = setting_json_fixture()
      update_attrs = %{password: "some updated password", serverName: "some updated serverName", adminPassword: "some updated adminPassword", trackMedalsRequirement: 43, safetyRatingRequirement: 43, racecraftRatingRequirement: 43, spectatorPassword: "some updated spectatorPassword", maxCarSlots: 43, dumpLeaderboards: false, isRaceLocked: false, randomizeTrackWhenEmpty: false, centralEntryListPath: "some updated centralEntryListPath", allowAutoDQ: false, shortFormationLap: false, dumpEntryList: false, ignorePrematureDisconnects: false}

      assert {:ok, %SettingJSON{} = setting_json} = Servers.update_setting_json(setting_json, update_attrs)
      assert setting_json.password == "some updated password"
      assert setting_json.serverName == "some updated serverName"
      assert setting_json.adminPassword == "some updated adminPassword"
      assert setting_json.trackMedalsRequirement == 43
      assert setting_json.safetyRatingRequirement == 43
      assert setting_json.racecraftRatingRequirement == 43
      assert setting_json.spectatorPassword == "some updated spectatorPassword"
      assert setting_json.maxCarSlots == 43
      assert setting_json.dumpLeaderboards == false
      assert setting_json.isRaceLocked == false
      assert setting_json.randomizeTrackWhenEmpty == false
      assert setting_json.centralEntryListPath == "some updated centralEntryListPath"
      assert setting_json.allowAutoDQ == false
      assert setting_json.shortFormationLap == false
      assert setting_json.dumpEntryList == false
      assert setting_json.ignorePrematureDisconnects == false
    end

    test "update_setting_json/2 with invalid data returns error changeset" do
      setting_json = setting_json_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_setting_json(setting_json, @invalid_attrs)
      assert setting_json == Servers.get_setting_json!(setting_json.id)
    end

    test "delete_setting_json/1 deletes the setting_json" do
      setting_json = setting_json_fixture()
      assert {:ok, %SettingJSON{}} = Servers.delete_setting_json(setting_json)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_setting_json!(setting_json.id) end
    end

    test "change_setting_json/1 returns a setting_json changeset" do
      setting_json = setting_json_fixture()
      assert %Ecto.Changeset{} = Servers.change_setting_json(setting_json)
    end
  end

  describe "session_types" do
    alias Boxboxbox.Servers.SessionType

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{name: nil, value: nil}

    test "list_session_types/0 returns all session_types" do
      session_type = session_type_fixture()
      assert Servers.list_session_types() == [session_type]
    end

    test "get_session_type!/1 returns the session_type with given id" do
      session_type = session_type_fixture()
      assert Servers.get_session_type!(session_type.id) == session_type
    end

    test "create_session_type/1 with valid data creates a session_type" do
      valid_attrs = %{name: "some name", value: "some value"}

      assert {:ok, %SessionType{} = session_type} = Servers.create_session_type(valid_attrs)
      assert session_type.name == "some name"
      assert session_type.value == "some value"
    end

    test "create_session_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_session_type(@invalid_attrs)
    end

    test "update_session_type/2 with valid data updates the session_type" do
      session_type = session_type_fixture()
      update_attrs = %{name: "some updated name", value: "some updated value"}

      assert {:ok, %SessionType{} = session_type} = Servers.update_session_type(session_type, update_attrs)
      assert session_type.name == "some updated name"
      assert session_type.value == "some updated value"
    end

    test "update_session_type/2 with invalid data returns error changeset" do
      session_type = session_type_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_session_type(session_type, @invalid_attrs)
      assert session_type == Servers.get_session_type!(session_type.id)
    end

    test "delete_session_type/1 deletes the session_type" do
      session_type = session_type_fixture()
      assert {:ok, %SessionType{}} = Servers.delete_session_type(session_type)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_session_type!(session_type.id) end
    end

    test "change_session_type/1 returns a session_type changeset" do
      session_type = session_type_fixture()
      assert %Ecto.Changeset{} = Servers.change_session_type(session_type)
    end
  end

  describe "sessions" do
    alias Boxboxbox.Servers.Session

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{hourOfDay: nil, dayOfWeekend: nil, timeMultiplier: nil, sessionDurationMinutes: nil}

    test "list_sessions/0 returns all sessions" do
      session = session_fixture()
      assert Servers.list_sessions() == [session]
    end

    test "get_session!/1 returns the session with given id" do
      session = session_fixture()
      assert Servers.get_session!(session.id) == session
    end

    test "create_session/1 with valid data creates a session" do
      valid_attrs = %{hourOfDay: 42, dayOfWeekend: 42, timeMultiplier: 42, sessionDurationMinutes: 42}

      assert {:ok, %Session{} = session} = Servers.create_session(valid_attrs)
      assert session.hourOfDay == 42
      assert session.dayOfWeekend == 42
      assert session.timeMultiplier == 42
      assert session.sessionDurationMinutes == 42
    end

    test "create_session/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_session(@invalid_attrs)
    end

    test "update_session/2 with valid data updates the session" do
      session = session_fixture()
      update_attrs = %{hourOfDay: 43, dayOfWeekend: 43, timeMultiplier: 43, sessionDurationMinutes: 43}

      assert {:ok, %Session{} = session} = Servers.update_session(session, update_attrs)
      assert session.hourOfDay == 43
      assert session.dayOfWeekend == 43
      assert session.timeMultiplier == 43
      assert session.sessionDurationMinutes == 43
    end

    test "update_session/2 with invalid data returns error changeset" do
      session = session_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_session(session, @invalid_attrs)
      assert session == Servers.get_session!(session.id)
    end

    test "delete_session/1 deletes the session" do
      session = session_fixture()
      assert {:ok, %Session{}} = Servers.delete_session(session)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_session!(session.id) end
    end

    test "change_session/1 returns a session changeset" do
      session = session_fixture()
      assert %Ecto.Changeset{} = Servers.change_session(session)
    end
  end

  describe "event_jsons" do
    alias Boxboxbox.Servers.EventJSON

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{preRaceWaitingTimeSeconds: nil, sessionOverTimeSeconds: nil, ambiantTemp: nil, cloudLevel: nil, rain: nil, weatherRandomness: nil, postQualySeconds: nil, postRaceSeconds: nil, metaData: nil}

    test "list_event_jsons/0 returns all event_jsons" do
      event_json = event_json_fixture()
      assert Servers.list_event_jsons() == [event_json]
    end

    test "get_event_json!/1 returns the event_json with given id" do
      event_json = event_json_fixture()
      assert Servers.get_event_json!(event_json.id) == event_json
    end

    test "create_event_json/1 with valid data creates a event_json" do
      valid_attrs = %{preRaceWaitingTimeSeconds: 42, sessionOverTimeSeconds: 42, ambiantTemp: 42, cloudLevel: 120.5, rain: 120.5, weatherRandomness: 42, postQualySeconds: 42, postRaceSeconds: 42, metaData: "some metaData"}

      assert {:ok, %EventJSON{} = event_json} = Servers.create_event_json(valid_attrs)
      assert event_json.preRaceWaitingTimeSeconds == 42
      assert event_json.sessionOverTimeSeconds == 42
      assert event_json.ambiantTemp == 42
      assert event_json.cloudLevel == 120.5
      assert event_json.rain == 120.5
      assert event_json.weatherRandomness == 42
      assert event_json.postQualySeconds == 42
      assert event_json.postRaceSeconds == 42
      assert event_json.metaData == "some metaData"
    end

    test "create_event_json/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_event_json(@invalid_attrs)
    end

    test "update_event_json/2 with valid data updates the event_json" do
      event_json = event_json_fixture()
      update_attrs = %{preRaceWaitingTimeSeconds: 43, sessionOverTimeSeconds: 43, ambiantTemp: 43, cloudLevel: 456.7, rain: 456.7, weatherRandomness: 43, postQualySeconds: 43, postRaceSeconds: 43, metaData: "some updated metaData"}

      assert {:ok, %EventJSON{} = event_json} = Servers.update_event_json(event_json, update_attrs)
      assert event_json.preRaceWaitingTimeSeconds == 43
      assert event_json.sessionOverTimeSeconds == 43
      assert event_json.ambiantTemp == 43
      assert event_json.cloudLevel == 456.7
      assert event_json.rain == 456.7
      assert event_json.weatherRandomness == 43
      assert event_json.postQualySeconds == 43
      assert event_json.postRaceSeconds == 43
      assert event_json.metaData == "some updated metaData"
    end

    test "update_event_json/2 with invalid data returns error changeset" do
      event_json = event_json_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_event_json(event_json, @invalid_attrs)
      assert event_json == Servers.get_event_json!(event_json.id)
    end

    test "delete_event_json/1 deletes the event_json" do
      event_json = event_json_fixture()
      assert {:ok, %EventJSON{}} = Servers.delete_event_json(event_json)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_event_json!(event_json.id) end
    end

    test "change_event_json/1 returns a event_json changeset" do
      event_json = event_json_fixture()
      assert %Ecto.Changeset{} = Servers.change_event_json(event_json)
    end
  end

  describe "qualify_types" do
    alias Boxboxbox.Servers.QualifyType

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{enabled: nil, name: nil}

    test "list_qualify_types/0 returns all qualify_types" do
      qualify_type = qualify_type_fixture()
      assert Servers.list_qualify_types() == [qualify_type]
    end

    test "get_qualify_type!/1 returns the qualify_type with given id" do
      qualify_type = qualify_type_fixture()
      assert Servers.get_qualify_type!(qualify_type.id) == qualify_type
    end

    test "create_qualify_type/1 with valid data creates a qualify_type" do
      valid_attrs = %{enabled: true, name: "some name"}

      assert {:ok, %QualifyType{} = qualify_type} = Servers.create_qualify_type(valid_attrs)
      assert qualify_type.enabled == true
      assert qualify_type.name == "some name"
    end

    test "create_qualify_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_qualify_type(@invalid_attrs)
    end

    test "update_qualify_type/2 with valid data updates the qualify_type" do
      qualify_type = qualify_type_fixture()
      update_attrs = %{enabled: false, name: "some updated name"}

      assert {:ok, %QualifyType{} = qualify_type} = Servers.update_qualify_type(qualify_type, update_attrs)
      assert qualify_type.enabled == false
      assert qualify_type.name == "some updated name"
    end

    test "update_qualify_type/2 with invalid data returns error changeset" do
      qualify_type = qualify_type_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_qualify_type(qualify_type, @invalid_attrs)
      assert qualify_type == Servers.get_qualify_type!(qualify_type.id)
    end

    test "delete_qualify_type/1 deletes the qualify_type" do
      qualify_type = qualify_type_fixture()
      assert {:ok, %QualifyType{}} = Servers.delete_qualify_type(qualify_type)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_qualify_type!(qualify_type.id) end
    end

    test "change_qualify_type/1 returns a qualify_type changeset" do
      qualify_type = qualify_type_fixture()
      assert %Ecto.Changeset{} = Servers.change_qualify_type(qualify_type)
    end
  end

  describe "event_rule_jsons" do
    alias Boxboxbox.Servers.EventRuleJSON

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{pitWindowLengthSec: nil, driverStintTimeSec: nil, mandatoryPitstopCount: nil, maxTotalDrivingTime: nil, maxDriversCount: nil, isRefuellingAllowedInRace: nil, isRefuellingTimeFixed: nil, isMandatoryPitstopRefuellingRequired: nil, isMandatoryPitstopTyreChangeRequired: nil, isMandatoryPitstopSwapDriverRequired: nil, tyreSetCount: nil}

    test "list_event_rule_jsons/0 returns all event_rule_jsons" do
      event_rule_json = event_rule_json_fixture()
      assert Servers.list_event_rule_jsons() == [event_rule_json]
    end

    test "get_event_rule_json!/1 returns the event_rule_json with given id" do
      event_rule_json = event_rule_json_fixture()
      assert Servers.get_event_rule_json!(event_rule_json.id) == event_rule_json
    end

    test "create_event_rule_json/1 with valid data creates a event_rule_json" do
      valid_attrs = %{pitWindowLengthSec: 42, driverStintTimeSec: 42, mandatoryPitstopCount: 42, maxTotalDrivingTime: 42, maxDriversCount: 42, isRefuellingAllowedInRace: true, isRefuellingTimeFixed: true, isMandatoryPitstopRefuellingRequired: true, isMandatoryPitstopTyreChangeRequired: true, isMandatoryPitstopSwapDriverRequired: true, tyreSetCount: 42}

      assert {:ok, %EventRuleJSON{} = event_rule_json} = Servers.create_event_rule_json(valid_attrs)
      assert event_rule_json.pitWindowLengthSec == 42
      assert event_rule_json.driverStintTimeSec == 42
      assert event_rule_json.mandatoryPitstopCount == 42
      assert event_rule_json.maxTotalDrivingTime == 42
      assert event_rule_json.maxDriversCount == 42
      assert event_rule_json.isRefuellingAllowedInRace == true
      assert event_rule_json.isRefuellingTimeFixed == true
      assert event_rule_json.isMandatoryPitstopRefuellingRequired == true
      assert event_rule_json.isMandatoryPitstopTyreChangeRequired == true
      assert event_rule_json.isMandatoryPitstopSwapDriverRequired == true
      assert event_rule_json.tyreSetCount == 42
    end

    test "create_event_rule_json/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_event_rule_json(@invalid_attrs)
    end

    test "update_event_rule_json/2 with valid data updates the event_rule_json" do
      event_rule_json = event_rule_json_fixture()
      update_attrs = %{pitWindowLengthSec: 43, driverStintTimeSec: 43, mandatoryPitstopCount: 43, maxTotalDrivingTime: 43, maxDriversCount: 43, isRefuellingAllowedInRace: false, isRefuellingTimeFixed: false, isMandatoryPitstopRefuellingRequired: false, isMandatoryPitstopTyreChangeRequired: false, isMandatoryPitstopSwapDriverRequired: false, tyreSetCount: 43}

      assert {:ok, %EventRuleJSON{} = event_rule_json} = Servers.update_event_rule_json(event_rule_json, update_attrs)
      assert event_rule_json.pitWindowLengthSec == 43
      assert event_rule_json.driverStintTimeSec == 43
      assert event_rule_json.mandatoryPitstopCount == 43
      assert event_rule_json.maxTotalDrivingTime == 43
      assert event_rule_json.maxDriversCount == 43
      assert event_rule_json.isRefuellingAllowedInRace == false
      assert event_rule_json.isRefuellingTimeFixed == false
      assert event_rule_json.isMandatoryPitstopRefuellingRequired == false
      assert event_rule_json.isMandatoryPitstopTyreChangeRequired == false
      assert event_rule_json.isMandatoryPitstopSwapDriverRequired == false
      assert event_rule_json.tyreSetCount == 43
    end

    test "update_event_rule_json/2 with invalid data returns error changeset" do
      event_rule_json = event_rule_json_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_event_rule_json(event_rule_json, @invalid_attrs)
      assert event_rule_json == Servers.get_event_rule_json!(event_rule_json.id)
    end

    test "delete_event_rule_json/1 deletes the event_rule_json" do
      event_rule_json = event_rule_json_fixture()
      assert {:ok, %EventRuleJSON{}} = Servers.delete_event_rule_json(event_rule_json)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_event_rule_json!(event_rule_json.id) end
    end

    test "change_event_rule_json/1 returns a event_rule_json changeset" do
      event_rule_json = event_rule_json_fixture()
      assert %Ecto.Changeset{} = Servers.change_event_rule_json(event_rule_json)
    end
  end

  describe "assist_rule_jsons" do
    alias Boxboxbox.Servers.AssistRulesJSON

    import Boxboxbox.ServersFixtures

    @invalid_attrs %{stabilityControlLevelMax: nil, disableAutosteer: nil, disableAutoLights: nil, disableAutoWiper: nil, disableAutoEngineStart: nil, disableAutoPitLimiter: nil, disableAutoClutch: nil, disableAutoGear: nil, disableIdealLine: nil}

    test "list_assist_rule_jsons/0 returns all assist_rule_jsons" do
      assist_rules_json = assist_rules_json_fixture()
      assert Servers.list_assist_rule_jsons() == [assist_rules_json]
    end

    test "get_assist_rules_json!/1 returns the assist_rules_json with given id" do
      assist_rules_json = assist_rules_json_fixture()
      assert Servers.get_assist_rules_json!(assist_rules_json.id) == assist_rules_json
    end

    test "create_assist_rules_json/1 with valid data creates a assist_rules_json" do
      valid_attrs = %{stabilityControlLevelMax: 42, disableAutosteer: true, disableAutoLights: true, disableAutoWiper: true, disableAutoEngineStart: true, disableAutoPitLimiter: true, disableAutoClutch: true, disableAutoGear: true, disableIdealLine: true}

      assert {:ok, %AssistRulesJSON{} = assist_rules_json} = Servers.create_assist_rules_json(valid_attrs)
      assert assist_rules_json.stabilityControlLevelMax == 42
      assert assist_rules_json.disableAutosteer == true
      assert assist_rules_json.disableAutoLights == true
      assert assist_rules_json.disableAutoWiper == true
      assert assist_rules_json.disableAutoEngineStart == true
      assert assist_rules_json.disableAutoPitLimiter == true
      assert assist_rules_json.disableAutoClutch == true
      assert assist_rules_json.disableAutoGear == true
      assert assist_rules_json.disableIdealLine == true
    end

    test "create_assist_rules_json/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Servers.create_assist_rules_json(@invalid_attrs)
    end

    test "update_assist_rules_json/2 with valid data updates the assist_rules_json" do
      assist_rules_json = assist_rules_json_fixture()
      update_attrs = %{stabilityControlLevelMax: 43, disableAutosteer: false, disableAutoLights: false, disableAutoWiper: false, disableAutoEngineStart: false, disableAutoPitLimiter: false, disableAutoClutch: false, disableAutoGear: false, disableIdealLine: false}

      assert {:ok, %AssistRulesJSON{} = assist_rules_json} = Servers.update_assist_rules_json(assist_rules_json, update_attrs)
      assert assist_rules_json.stabilityControlLevelMax == 43
      assert assist_rules_json.disableAutosteer == false
      assert assist_rules_json.disableAutoLights == false
      assert assist_rules_json.disableAutoWiper == false
      assert assist_rules_json.disableAutoEngineStart == false
      assert assist_rules_json.disableAutoPitLimiter == false
      assert assist_rules_json.disableAutoClutch == false
      assert assist_rules_json.disableAutoGear == false
      assert assist_rules_json.disableIdealLine == false
    end

    test "update_assist_rules_json/2 with invalid data returns error changeset" do
      assist_rules_json = assist_rules_json_fixture()
      assert {:error, %Ecto.Changeset{}} = Servers.update_assist_rules_json(assist_rules_json, @invalid_attrs)
      assert assist_rules_json == Servers.get_assist_rules_json!(assist_rules_json.id)
    end

    test "delete_assist_rules_json/1 deletes the assist_rules_json" do
      assist_rules_json = assist_rules_json_fixture()
      assert {:ok, %AssistRulesJSON{}} = Servers.delete_assist_rules_json(assist_rules_json)
      assert_raise Ecto.NoResultsError, fn -> Servers.get_assist_rules_json!(assist_rules_json.id) end
    end

    test "change_assist_rules_json/1 returns a assist_rules_json changeset" do
      assist_rules_json = assist_rules_json_fixture()
      assert %Ecto.Changeset{} = Servers.change_assist_rules_json(assist_rules_json)
    end
  end
end
