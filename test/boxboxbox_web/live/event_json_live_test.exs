defmodule BoxboxboxWeb.EventJSONLiveTest do
  use BoxboxboxWeb.ConnCase

  import Phoenix.LiveViewTest
  import Boxboxbox.ServersFixtures

  @create_attrs %{preRaceWaitingTimeSeconds: 42, sessionOverTimeSeconds: 42, ambiantTemp: 42, cloudLevel: 120.5, rain: 120.5, weatherRandomness: 42, postQualySeconds: 42, postRaceSeconds: 42, metaData: "some metaData"}
  @update_attrs %{preRaceWaitingTimeSeconds: 43, sessionOverTimeSeconds: 43, ambiantTemp: 43, cloudLevel: 456.7, rain: 456.7, weatherRandomness: 43, postQualySeconds: 43, postRaceSeconds: 43, metaData: "some updated metaData"}
  @invalid_attrs %{preRaceWaitingTimeSeconds: nil, sessionOverTimeSeconds: nil, ambiantTemp: nil, cloudLevel: nil, rain: nil, weatherRandomness: nil, postQualySeconds: nil, postRaceSeconds: nil, metaData: nil}

  defp create_event_json(_) do
    event_json = event_json_fixture()
    %{event_json: event_json}
  end

  describe "Index" do
    setup [:create_event_json]

    test "lists all event_jsons", %{conn: conn, event_json: event_json} do
      {:ok, _index_live, html} = live(conn, ~p"/event_jsons")

      assert html =~ "Listing Event jsons"
      assert html =~ event_json.metaData
    end

    test "saves new event_json", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/event_jsons")

      assert index_live |> element("a", "New Event json") |> render_click() =~
               "New Event json"

      assert_patch(index_live, ~p"/event_jsons/new")

      assert index_live
             |> form("#event_json-form", event_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#event_json-form", event_json: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/event_jsons")

      html = render(index_live)
      assert html =~ "Event json created successfully"
      assert html =~ "some metaData"
    end

    test "updates event_json in listing", %{conn: conn, event_json: event_json} do
      {:ok, index_live, _html} = live(conn, ~p"/event_jsons")

      assert index_live |> element("#event_jsons-#{event_json.id} a", "Edit") |> render_click() =~
               "Edit Event json"

      assert_patch(index_live, ~p"/event_jsons/#{event_json}/edit")

      assert index_live
             |> form("#event_json-form", event_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#event_json-form", event_json: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/event_jsons")

      html = render(index_live)
      assert html =~ "Event json updated successfully"
      assert html =~ "some updated metaData"
    end

    test "deletes event_json in listing", %{conn: conn, event_json: event_json} do
      {:ok, index_live, _html} = live(conn, ~p"/event_jsons")

      assert index_live |> element("#event_jsons-#{event_json.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#event_jsons-#{event_json.id}")
    end
  end

  describe "Show" do
    setup [:create_event_json]

    test "displays event_json", %{conn: conn, event_json: event_json} do
      {:ok, _show_live, html} = live(conn, ~p"/event_jsons/#{event_json}")

      assert html =~ "Show Event json"
      assert html =~ event_json.metaData
    end

    test "updates event_json within modal", %{conn: conn, event_json: event_json} do
      {:ok, show_live, _html} = live(conn, ~p"/event_jsons/#{event_json}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Event json"

      assert_patch(show_live, ~p"/event_jsons/#{event_json}/show/edit")

      assert show_live
             |> form("#event_json-form", event_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#event_json-form", event_json: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/event_jsons/#{event_json}")

      html = render(show_live)
      assert html =~ "Event json updated successfully"
      assert html =~ "some updated metaData"
    end
  end
end
