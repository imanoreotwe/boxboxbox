defmodule BoxboxboxWeb.SettingJSONLiveTest do
  use BoxboxboxWeb.ConnCase

  import Phoenix.LiveViewTest
  import Boxboxbox.ServersFixtures

  @create_attrs %{password: "some password", serverName: "some serverName", adminPassword: "some adminPassword", trackMedalsRequirement: 42, safetyRatingRequirement: 42, racecraftRatingRequirement: 42, spectatorPassword: "some spectatorPassword", maxCarSlots: 42, dumpLeaderboards: true, isRaceLocked: true, randomizeTrackWhenEmpty: true, centralEntryListPath: "some centralEntryListPath", allowAutoDQ: true, shortFormationLap: true, dumpEntryList: true, ignorePrematureDisconnects: true}
  @update_attrs %{password: "some updated password", serverName: "some updated serverName", adminPassword: "some updated adminPassword", trackMedalsRequirement: 43, safetyRatingRequirement: 43, racecraftRatingRequirement: 43, spectatorPassword: "some updated spectatorPassword", maxCarSlots: 43, dumpLeaderboards: false, isRaceLocked: false, randomizeTrackWhenEmpty: false, centralEntryListPath: "some updated centralEntryListPath", allowAutoDQ: false, shortFormationLap: false, dumpEntryList: false, ignorePrematureDisconnects: false}
  @invalid_attrs %{password: nil, serverName: nil, adminPassword: nil, trackMedalsRequirement: nil, safetyRatingRequirement: nil, racecraftRatingRequirement: nil, spectatorPassword: nil, maxCarSlots: nil, dumpLeaderboards: false, isRaceLocked: false, randomizeTrackWhenEmpty: false, centralEntryListPath: nil, allowAutoDQ: false, shortFormationLap: false, dumpEntryList: false, ignorePrematureDisconnects: false}

  defp create_setting_json(_) do
    setting_json = setting_json_fixture()
    %{setting_json: setting_json}
  end

  describe "Index" do
    setup [:create_setting_json]

    test "lists all setting_jsons", %{conn: conn, setting_json: setting_json} do
      {:ok, _index_live, html} = live(conn, ~p"/setting_jsons")

      assert html =~ "Listing Setting jsons"
      assert html =~ setting_json.password
    end

    test "saves new setting_json", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/setting_jsons")

      assert index_live |> element("a", "New Setting json") |> render_click() =~
               "New Setting json"

      assert_patch(index_live, ~p"/setting_jsons/new")

      assert index_live
             |> form("#setting_json-form", setting_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#setting_json-form", setting_json: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/setting_jsons")

      html = render(index_live)
      assert html =~ "Setting json created successfully"
      assert html =~ "some password"
    end

    test "updates setting_json in listing", %{conn: conn, setting_json: setting_json} do
      {:ok, index_live, _html} = live(conn, ~p"/setting_jsons")

      assert index_live |> element("#setting_jsons-#{setting_json.id} a", "Edit") |> render_click() =~
               "Edit Setting json"

      assert_patch(index_live, ~p"/setting_jsons/#{setting_json}/edit")

      assert index_live
             |> form("#setting_json-form", setting_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#setting_json-form", setting_json: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/setting_jsons")

      html = render(index_live)
      assert html =~ "Setting json updated successfully"
      assert html =~ "some updated password"
    end

    test "deletes setting_json in listing", %{conn: conn, setting_json: setting_json} do
      {:ok, index_live, _html} = live(conn, ~p"/setting_jsons")

      assert index_live |> element("#setting_jsons-#{setting_json.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#setting_jsons-#{setting_json.id}")
    end
  end

  describe "Show" do
    setup [:create_setting_json]

    test "displays setting_json", %{conn: conn, setting_json: setting_json} do
      {:ok, _show_live, html} = live(conn, ~p"/setting_jsons/#{setting_json}")

      assert html =~ "Show Setting json"
      assert html =~ setting_json.password
    end

    test "updates setting_json within modal", %{conn: conn, setting_json: setting_json} do
      {:ok, show_live, _html} = live(conn, ~p"/setting_jsons/#{setting_json}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Setting json"

      assert_patch(show_live, ~p"/setting_jsons/#{setting_json}/show/edit")

      assert show_live
             |> form("#setting_json-form", setting_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#setting_json-form", setting_json: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/setting_jsons/#{setting_json}")

      html = render(show_live)
      assert html =~ "Setting json updated successfully"
      assert html =~ "some updated password"
    end
  end
end
