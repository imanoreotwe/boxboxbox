defmodule BoxboxboxWeb.AssistRulesJSONLiveTest do
  use BoxboxboxWeb.ConnCase

  import Phoenix.LiveViewTest
  import Boxboxbox.ServersFixtures

  @create_attrs %{stabilityControlLevelMax: 42, disableAutosteer: true, disableAutoLights: true, disableAutoWiper: true, disableAutoEngineStart: true, disableAutoPitLimiter: true, disableAutoClutch: true, disableAutoGear: true, disableIdealLine: true}
  @update_attrs %{stabilityControlLevelMax: 43, disableAutosteer: false, disableAutoLights: false, disableAutoWiper: false, disableAutoEngineStart: false, disableAutoPitLimiter: false, disableAutoClutch: false, disableAutoGear: false, disableIdealLine: false}
  @invalid_attrs %{stabilityControlLevelMax: nil, disableAutosteer: false, disableAutoLights: false, disableAutoWiper: false, disableAutoEngineStart: false, disableAutoPitLimiter: false, disableAutoClutch: false, disableAutoGear: false, disableIdealLine: false}

  defp create_assist_rules_json(_) do
    assist_rules_json = assist_rules_json_fixture()
    %{assist_rules_json: assist_rules_json}
  end

  describe "Index" do
    setup [:create_assist_rules_json]

    test "lists all assist_rule_jsons", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/assist_rule_jsons")

      assert html =~ "Listing Assist rule jsons"
    end

    test "saves new assist_rules_json", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/assist_rule_jsons")

      assert index_live |> element("a", "New Assist rules json") |> render_click() =~
               "New Assist rules json"

      assert_patch(index_live, ~p"/assist_rule_jsons/new")

      assert index_live
             |> form("#assist_rules_json-form", assist_rules_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#assist_rules_json-form", assist_rules_json: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/assist_rule_jsons")

      html = render(index_live)
      assert html =~ "Assist rules json created successfully"
    end

    test "updates assist_rules_json in listing", %{conn: conn, assist_rules_json: assist_rules_json} do
      {:ok, index_live, _html} = live(conn, ~p"/assist_rule_jsons")

      assert index_live |> element("#assist_rule_jsons-#{assist_rules_json.id} a", "Edit") |> render_click() =~
               "Edit Assist rules json"

      assert_patch(index_live, ~p"/assist_rule_jsons/#{assist_rules_json}/edit")

      assert index_live
             |> form("#assist_rules_json-form", assist_rules_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#assist_rules_json-form", assist_rules_json: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/assist_rule_jsons")

      html = render(index_live)
      assert html =~ "Assist rules json updated successfully"
    end

    test "deletes assist_rules_json in listing", %{conn: conn, assist_rules_json: assist_rules_json} do
      {:ok, index_live, _html} = live(conn, ~p"/assist_rule_jsons")

      assert index_live |> element("#assist_rule_jsons-#{assist_rules_json.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#assist_rule_jsons-#{assist_rules_json.id}")
    end
  end

  describe "Show" do
    setup [:create_assist_rules_json]

    test "displays assist_rules_json", %{conn: conn, assist_rules_json: assist_rules_json} do
      {:ok, _show_live, html} = live(conn, ~p"/assist_rule_jsons/#{assist_rules_json}")

      assert html =~ "Show Assist rules json"
    end

    test "updates assist_rules_json within modal", %{conn: conn, assist_rules_json: assist_rules_json} do
      {:ok, show_live, _html} = live(conn, ~p"/assist_rule_jsons/#{assist_rules_json}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Assist rules json"

      assert_patch(show_live, ~p"/assist_rule_jsons/#{assist_rules_json}/show/edit")

      assert show_live
             |> form("#assist_rules_json-form", assist_rules_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#assist_rules_json-form", assist_rules_json: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/assist_rule_jsons/#{assist_rules_json}")

      html = render(show_live)
      assert html =~ "Assist rules json updated successfully"
    end
  end
end
