defmodule BoxboxboxWeb.ConfigurationJSONLiveTest do
  use BoxboxboxWeb.ConnCase

  import Phoenix.LiveViewTest
  import Boxboxbox.ServersFixtures

  @create_attrs %{udpPort: 42, tcpPort: 42, registerToLobby: true, maxConnections: 42, lanDiscovery: true, publicIP: "some publicIP"}
  @update_attrs %{udpPort: 43, tcpPort: 43, registerToLobby: false, maxConnections: 43, lanDiscovery: false, publicIP: "some updated publicIP"}
  @invalid_attrs %{udpPort: nil, tcpPort: nil, registerToLobby: false, maxConnections: nil, lanDiscovery: false, publicIP: nil}

  defp create_configuration_json(_) do
    configuration_json = configuration_json_fixture()
    %{configuration_json: configuration_json}
  end

  describe "Index" do
    setup [:create_configuration_json]

    test "lists all configuration_jsons", %{conn: conn, configuration_json: configuration_json} do
      {:ok, _index_live, html} = live(conn, ~p"/configuration_jsons")

      assert html =~ "Listing Configuration jsons"
      assert html =~ configuration_json.publicIP
    end

    test "saves new configuration_json", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/configuration_jsons")

      assert index_live |> element("a", "New Configuration json") |> render_click() =~
               "New Configuration json"

      assert_patch(index_live, ~p"/configuration_jsons/new")

      assert index_live
             |> form("#configuration_json-form", configuration_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#configuration_json-form", configuration_json: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/configuration_jsons")

      html = render(index_live)
      assert html =~ "Configuration json created successfully"
      assert html =~ "some publicIP"
    end

    test "updates configuration_json in listing", %{conn: conn, configuration_json: configuration_json} do
      {:ok, index_live, _html} = live(conn, ~p"/configuration_jsons")

      assert index_live |> element("#configuration_jsons-#{configuration_json.id} a", "Edit") |> render_click() =~
               "Edit Configuration json"

      assert_patch(index_live, ~p"/configuration_jsons/#{configuration_json}/edit")

      assert index_live
             |> form("#configuration_json-form", configuration_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#configuration_json-form", configuration_json: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/configuration_jsons")

      html = render(index_live)
      assert html =~ "Configuration json updated successfully"
      assert html =~ "some updated publicIP"
    end

    test "deletes configuration_json in listing", %{conn: conn, configuration_json: configuration_json} do
      {:ok, index_live, _html} = live(conn, ~p"/configuration_jsons")

      assert index_live |> element("#configuration_jsons-#{configuration_json.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#configuration_jsons-#{configuration_json.id}")
    end
  end

  describe "Show" do
    setup [:create_configuration_json]

    test "displays configuration_json", %{conn: conn, configuration_json: configuration_json} do
      {:ok, _show_live, html} = live(conn, ~p"/configuration_jsons/#{configuration_json}")

      assert html =~ "Show Configuration json"
      assert html =~ configuration_json.publicIP
    end

    test "updates configuration_json within modal", %{conn: conn, configuration_json: configuration_json} do
      {:ok, show_live, _html} = live(conn, ~p"/configuration_jsons/#{configuration_json}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Configuration json"

      assert_patch(show_live, ~p"/configuration_jsons/#{configuration_json}/show/edit")

      assert show_live
             |> form("#configuration_json-form", configuration_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#configuration_json-form", configuration_json: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/configuration_jsons/#{configuration_json}")

      html = render(show_live)
      assert html =~ "Configuration json updated successfully"
      assert html =~ "some updated publicIP"
    end
  end
end
