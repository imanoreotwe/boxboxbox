defmodule BoxboxboxWeb.EventRuleJSONLiveTest do
  use BoxboxboxWeb.ConnCase

  import Phoenix.LiveViewTest
  import Boxboxbox.ServersFixtures

  @create_attrs %{pitWindowLengthSec: 42, driverStintTimeSec: 42, mandatoryPitstopCount: 42, maxTotalDrivingTime: 42, maxDriversCount: 42, isRefuellingAllowedInRace: true, isRefuellingTimeFixed: true, isMandatoryPitstopRefuellingRequired: true, isMandatoryPitstopTyreChangeRequired: true, isMandatoryPitstopSwapDriverRequired: true, tyreSetCount: 42}
  @update_attrs %{pitWindowLengthSec: 43, driverStintTimeSec: 43, mandatoryPitstopCount: 43, maxTotalDrivingTime: 43, maxDriversCount: 43, isRefuellingAllowedInRace: false, isRefuellingTimeFixed: false, isMandatoryPitstopRefuellingRequired: false, isMandatoryPitstopTyreChangeRequired: false, isMandatoryPitstopSwapDriverRequired: false, tyreSetCount: 43}
  @invalid_attrs %{pitWindowLengthSec: nil, driverStintTimeSec: nil, mandatoryPitstopCount: nil, maxTotalDrivingTime: nil, maxDriversCount: nil, isRefuellingAllowedInRace: false, isRefuellingTimeFixed: false, isMandatoryPitstopRefuellingRequired: false, isMandatoryPitstopTyreChangeRequired: false, isMandatoryPitstopSwapDriverRequired: false, tyreSetCount: nil}

  defp create_event_rule_json(_) do
    event_rule_json = event_rule_json_fixture()
    %{event_rule_json: event_rule_json}
  end

  describe "Index" do
    setup [:create_event_rule_json]

    test "lists all event_rule_jsons", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/event_rule_jsons")

      assert html =~ "Listing Event rule jsons"
    end

    test "saves new event_rule_json", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/event_rule_jsons")

      assert index_live |> element("a", "New Event rule json") |> render_click() =~
               "New Event rule json"

      assert_patch(index_live, ~p"/event_rule_jsons/new")

      assert index_live
             |> form("#event_rule_json-form", event_rule_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#event_rule_json-form", event_rule_json: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/event_rule_jsons")

      html = render(index_live)
      assert html =~ "Event rule json created successfully"
    end

    test "updates event_rule_json in listing", %{conn: conn, event_rule_json: event_rule_json} do
      {:ok, index_live, _html} = live(conn, ~p"/event_rule_jsons")

      assert index_live |> element("#event_rule_jsons-#{event_rule_json.id} a", "Edit") |> render_click() =~
               "Edit Event rule json"

      assert_patch(index_live, ~p"/event_rule_jsons/#{event_rule_json}/edit")

      assert index_live
             |> form("#event_rule_json-form", event_rule_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#event_rule_json-form", event_rule_json: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/event_rule_jsons")

      html = render(index_live)
      assert html =~ "Event rule json updated successfully"
    end

    test "deletes event_rule_json in listing", %{conn: conn, event_rule_json: event_rule_json} do
      {:ok, index_live, _html} = live(conn, ~p"/event_rule_jsons")

      assert index_live |> element("#event_rule_jsons-#{event_rule_json.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#event_rule_jsons-#{event_rule_json.id}")
    end
  end

  describe "Show" do
    setup [:create_event_rule_json]

    test "displays event_rule_json", %{conn: conn, event_rule_json: event_rule_json} do
      {:ok, _show_live, html} = live(conn, ~p"/event_rule_jsons/#{event_rule_json}")

      assert html =~ "Show Event rule json"
    end

    test "updates event_rule_json within modal", %{conn: conn, event_rule_json: event_rule_json} do
      {:ok, show_live, _html} = live(conn, ~p"/event_rule_jsons/#{event_rule_json}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Event rule json"

      assert_patch(show_live, ~p"/event_rule_jsons/#{event_rule_json}/show/edit")

      assert show_live
             |> form("#event_rule_json-form", event_rule_json: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#event_rule_json-form", event_rule_json: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/event_rule_jsons/#{event_rule_json}")

      html = render(show_live)
      assert html =~ "Event rule json updated successfully"
    end
  end
end
