defmodule Boxboxbox.CarsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Boxboxbox.Cars` context.
  """

  @doc """
  Generate a car_group.
  """
  def car_group_fixture(attrs \\ %{}) do
    {:ok, car_group} =
      attrs
      |> Enum.into(%{
        name: "some name",
        value: "some value"
      })
      |> Boxboxbox.Cars.create_car_group()

    car_group
  end
end
