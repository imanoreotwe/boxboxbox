defmodule Boxboxbox.ServersFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Boxboxbox.Servers` context.
  """

  @doc """
  Generate a configuration_json.
  """
  def configuration_json_fixture(attrs \\ %{}) do
    {:ok, configuration_json} =
      attrs
      |> Enum.into(%{
        lanDiscovery: true,
        maxConnections: 42,
        publicIP: "some publicIP",
        registerToLobby: true,
        tcpPort: 42,
        udpPort: 42
      })
      |> Boxboxbox.Servers.create_configuration_json()

    configuration_json
  end

  @doc """
  Generate a server.
  """
  def server_fixture(attrs \\ %{}) do
    {:ok, server} =
      attrs
      |> Enum.into(%{

      })
      |> Boxboxbox.Servers.create_server()

    server
  end

  @doc """
  Generate a setting_json.
  """
  def setting_json_fixture(attrs \\ %{}) do
    {:ok, setting_json} =
      attrs
      |> Enum.into(%{
        adminPassword: "some adminPassword",
        allowAutoDQ: true,
        centralEntryListPath: "some centralEntryListPath",
        dumpEntryList: true,
        dumpLeaderboards: true,
        ignorePrematureDisconnects: true,
        isRaceLocked: true,
        maxCarSlots: 42,
        password: "some password",
        racecraftRatingRequirement: 42,
        randomizeTrackWhenEmpty: true,
        safetyRatingRequirement: 42,
        serverName: "some serverName",
        shortFormationLap: true,
        spectatorPassword: "some spectatorPassword",
        trackMedalsRequirement: 42
      })
      |> Boxboxbox.Servers.create_setting_json()

    setting_json
  end

  @doc """
  Generate a session_type.
  """
  def session_type_fixture(attrs \\ %{}) do
    {:ok, session_type} =
      attrs
      |> Enum.into(%{
        name: "some name",
        value: "some value"
      })
      |> Boxboxbox.Servers.create_session_type()

    session_type
  end

  @doc """
  Generate a session.
  """
  def session_fixture(attrs \\ %{}) do
    {:ok, session} =
      attrs
      |> Enum.into(%{
        dayOfWeekend: 42,
        hourOfDay: 42,
        sessionDurationMinutes: 42,
        timeMultiplier: 42
      })
      |> Boxboxbox.Servers.create_session()

    session
  end

  @doc """
  Generate a event_json.
  """
  def event_json_fixture(attrs \\ %{}) do
    {:ok, event_json} =
      attrs
      |> Enum.into(%{
        ambiantTemp: 42,
        cloudLevel: 120.5,
        metaData: "some metaData",
        postQualySeconds: 42,
        postRaceSeconds: 42,
        preRaceWaitingTimeSeconds: 42,
        rain: 120.5,
        sessionOverTimeSeconds: 42,
        weatherRandomness: 42
      })
      |> Boxboxbox.Servers.create_event_json()

    event_json
  end

  @doc """
  Generate a qualify_type.
  """
  def qualify_type_fixture(attrs \\ %{}) do
    {:ok, qualify_type} =
      attrs
      |> Enum.into(%{
        enabled: true,
        name: "some name"
      })
      |> Boxboxbox.Servers.create_qualify_type()

    qualify_type
  end

  @doc """
  Generate a event_rule_json.
  """
  def event_rule_json_fixture(attrs \\ %{}) do
    {:ok, event_rule_json} =
      attrs
      |> Enum.into(%{
        driverStintTimeSec: 42,
        isMandatoryPitstopRefuellingRequired: true,
        isMandatoryPitstopSwapDriverRequired: true,
        isMandatoryPitstopTyreChangeRequired: true,
        isRefuellingAllowedInRace: true,
        isRefuellingTimeFixed: true,
        mandatoryPitstopCount: 42,
        maxDriversCount: 42,
        maxTotalDrivingTime: 42,
        pitWindowLengthSec: 42,
        tyreSetCount: 42
      })
      |> Boxboxbox.Servers.create_event_rule_json()

    event_rule_json
  end

  @doc """
  Generate a assist_rules_json.
  """
  def assist_rules_json_fixture(attrs \\ %{}) do
    {:ok, assist_rules_json} =
      attrs
      |> Enum.into(%{
        disableAutoClutch: true,
        disableAutoEngineStart: true,
        disableAutoGear: true,
        disableAutoLights: true,
        disableAutoPitLimiter: true,
        disableAutoWiper: true,
        disableAutosteer: true,
        disableIdealLine: true,
        stabilityControlLevelMax: 42
      })
      |> Boxboxbox.Servers.create_assist_rules_json()

    assist_rules_json
  end
end
