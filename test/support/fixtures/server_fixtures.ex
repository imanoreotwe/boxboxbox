defmodule Boxboxbox.ServerFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Boxboxbox.Server` context.
  """

  @doc """
  Generate a formation_lap_type.
  """
  def formation_lap_type_fixture(attrs \\ %{}) do
    {:ok, formation_lap_type} =
      attrs
      |> Enum.into(%{
        description: "some description",
        name: "some name",
        value: 42
      })
      |> Boxboxbox.Server.create_formation_lap_type()

    formation_lap_type
  end
end
