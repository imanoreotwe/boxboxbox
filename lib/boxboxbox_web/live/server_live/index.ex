defmodule BoxboxboxWeb.ServerLive.Index do
  use BoxboxboxWeb, :live_view

  alias Boxboxbox.Servers
  alias Boxboxbox.Servers.Server
  alias Boxboxbox.Servers.ConfigurationJSON
  alias Boxboxbox.Servers.SettingJSON
  alias Boxboxbox.Servers.EventJSON
  alias Boxboxbox.Servers.EventRuleJSON
  alias Boxboxbox.Servers.AssistRuleJSON
  alias Boxboxbox.Accounts
  alias Boxboxbox.DockerSupervisor
  alias Boxboxbox.Workers.Docker

  @impl true
  def mount(_params, session, socket) do
    user = Accounts.get_user_by_session_token(session["user_token"])
    {:ok,
    socket
    |> assign(:user, user)
    |> stream(:servers, Servers.list_servers())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    server = Servers.get_server!(id)
    socket
    |> assign(:page_title, "Edit Server")
    |> assign(:server, server)
    |> assign(:configuration_json, Servers.get_configuration_json!(server.configuration))
    |> assign(:setting_json, Servers.get_setting_json!(server.setting))
    |> assign(:event_json, Servers.get_event_json!(server.event))
    |> assign(:event_rule_json, Servers.get_event_rule_json!(server.event_rule))
    |> assign(:assist_rule_json, Servers.get_assist_rule_json!(server.assist_rule))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Server")
    |> assign(:server, %Server{})
    |> assign(:configuration_json, %ConfigurationJSON{})
    |> assign(:setting_json, %SettingJSON{})
    |> assign(:event_json, %EventJSON{})
    |> assign(:event_rule_json, %EventRuleJSON{})
    |> assign(:assist_rule_json, %AssistRuleJSON{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Servers")
    |> assign(:server, nil)
  end

  @impl true
  def handle_info({BoxboxboxWeb.ServerLive.FormComponent, {:saved, server}}, socket) do
    path = System.tmp_dir!()
    |> Path.join(random_string(8))
    File.mkdir!(path)

    IO.inspect(path)

    cfg_path = Path.join(path, "cfg")
    File.mkdir!(cfg_path)

    Servers.generate_config_files(server, cfg_path)

    # start docker compose w/ path
    {:noreply,
    socket
    |> assign(:server, server)
    |> put_flash(:info, "new server saved")
    |> stream_insert(:servers, server)
  }
  end

  @impl true
  def handle_info({BoxboxboxWeb.ConfigurationJSONLive.FormComponent, {:saved, config}}, socket) do
    server = socket.assigns.server
    |> Map.put(:configuration, config.id)
    {:noreply,
    socket
    |> assign(:server, server)
    |> put_flash(:info, "new config saved")
  }
  end

  @impl true
  def handle_info({BoxboxboxWeb.SettingJSONLive.FormComponent, {:saved, setting}}, socket) do
    server = socket.assigns.server
    |> Map.put(:setting, setting.id)
    {:noreply,
    socket
    |> assign(:server, server)
    |> put_flash(:info, "new settings saved")
  }
  end

  @impl true
  def handle_info({BoxboxboxWeb.EventJSONLive.FormComponent, {:saved, event}}, socket) do
    server = socket.assigns.server
    |> Map.put(:event, event.id)
    {:noreply,
    socket
    |> assign(:server, server)
    |> put_flash(:info, "new event saved")
  }
  end

  @impl true
  def handle_info({BoxboxboxWeb.EventRuleJSONLive.FormComponent, {:saved, event_rule}}, socket) do
    server = socket.assigns.server
    |> Map.put(:event_rule, event_rule.id)
    {:noreply,
    socket
    |> assign(:server, server)
    |> put_flash(:info, "new event rule saved")
  }
  end

  @impl true
  def handle_info({BoxboxboxWeb.AssistRuleJSONLive.FormComponent, {:saved, assist_rule}}, socket) do
    server = socket.assigns.server
    |> Map.put(:assist_rule, assist_rule.id)
    {:noreply,
    socket
    |> assign(:server, server)
    |> put_flash(:info, "new assist rule saved")
  }
  end

  @impl true
  def handle_info({_, {:error, changeset}}, socket) do
    IO.inspect(changeset)
    {:noreply,
    socket
    |> put_flash(:error, "could not save")
    }
  end

  @impl true
  def handle_event("container-go", %{"id" => id}, socket) do
    {:ok, docker} = DockerSupervisor.start_child(id, "/tmp/xV_XINOt")
    Docker.build_and_run(docker)
    {:noreply,
    socket
    |> put_flash(:info, "did a thing")
  }
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    server = Servers.get_server!(id)
    {:ok, _} = Servers.delete_server(server)

    {:noreply, stream_delete(socket, :servers, server)}
  end

  defp random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
  end
end
