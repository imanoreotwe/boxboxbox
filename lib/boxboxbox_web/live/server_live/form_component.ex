defmodule BoxboxboxWeb.ServerLive.FormComponent do
  use BoxboxboxWeb, :live_component

  alias Boxboxbox.Servers

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage server records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="server-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >

        <:actions>
          <.button phx-disable-with="Saving...">Save Server</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{server: server} = assigns, socket) do
    changeset = Servers.change_server(server)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", params, socket) do
    changeset =
      socket.assigns.server
      |> Servers.change_server(params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", params, socket) do
    save_server(socket, socket.assigns.action, params)
  end

  defp save_server(socket, :edit, server_params) do
    params = server_params
    |> Map.put("owner", socket.assigns.user.id)
    case Servers.update_server(socket.assigns.server, params) do
      {:ok, server} ->
        notify_parent({:saved, server})

        {:noreply,
         socket
        }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_server(socket, :new, _) do
    params = %{}
    |> Map.put("owner", socket.assigns.user.id)
    |> Map.put("configuration", socket.assigns.server.configuration)
    |> Map.put("setting", socket.assigns.server.setting)
    |> Map.put("event", socket.assigns.server.event)
    |> Map.put("event_rule", socket.assigns.server.event_rule)
    |> Map.put("assist_rule", socket.assigns.server.assist_rule)
    case Servers.create_server(params) do
      {:ok, server} ->
        notify_parent({:saved, server})

        {:noreply,
         socket
         #|> put_flash(:info, "Server created successfully")
         #|> push_patch(to: socket.assigns.patch)
        }

      {:error, %Ecto.Changeset{} = changeset} ->
        notify_parent({:error, changeset})
        {:noreply, socket}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
