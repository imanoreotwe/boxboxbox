defmodule BoxboxboxWeb.AssistRulesJSONLive.Show do
  use BoxboxboxWeb, :live_view

  alias Boxboxbox.Servers

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:assist_rules_json, Servers.get_assist_rules_json!(id))}
  end

  defp page_title(:show), do: "Show Assist rules json"
  defp page_title(:edit), do: "Edit Assist rules json"
end
