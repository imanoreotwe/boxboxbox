defmodule BoxboxboxWeb.AssistRuleJSONLive.Index do
  use BoxboxboxWeb, :live_view

  alias Boxboxbox.Servers
  alias Boxboxbox.Servers.AssistRuleJSON

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :assist_rule_jsons, Servers.list_assist_rule_jsons())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Assist rules json")
    |> assign(:assist_rules_json, Servers.get_assist_rules_json!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Assist rules json")
    |> assign(:assist_rules_json, %AssistRuleJSON{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Assist rule jsons")
    |> assign(:assist_rules_json, nil)
  end

  @impl true
  def handle_info({BoxboxboxWeb.AssistRuleJSONLive.FormComponent, {:saved, assist_rules_json}}, socket) do
    {:noreply, stream_insert(socket, :assist_rule_jsons, assist_rules_json)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    assist_rules_json = Servers.get_assist_rules_json!(id)
    {:ok, _} = Servers.delete_assist_rules_json(assist_rules_json)

    {:noreply, stream_delete(socket, :assist_rule_jsons, assist_rules_json)}
  end
end
