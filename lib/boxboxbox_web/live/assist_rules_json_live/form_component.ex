defmodule BoxboxboxWeb.AssistRuleJSONLive.FormComponent do
  use BoxboxboxWeb, :live_component

  alias Boxboxbox.Servers

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage assist_rules_json records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="assist_rules_json-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:stabilityControlLevelMax]} type="number" label="Stabilitycontrollevelmax" />
        <.input field={@form[:disableAutosteer]} type="checkbox" label="Disableautosteer" />
        <.input field={@form[:disableAutoLights]} type="checkbox" label="Disableautolights" />
        <.input field={@form[:disableAutoWiper]} type="checkbox" label="Disableautowiper" />
        <.input field={@form[:disableAutoEngineStart]} type="checkbox" label="Disableautoenginestart" />
        <.input field={@form[:disableAutoPitLimiter]} type="checkbox" label="Disableautopitlimiter" />
        <.input field={@form[:disableAutoClutch]} type="checkbox" label="Disableautoclutch" />
        <.input field={@form[:disableAutoGear]} type="checkbox" label="Disableautogear" />
        <.input field={@form[:disableIdealLine]} type="checkbox" label="Disableidealline" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Assist rules json</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{assist_rules_json: assist_rules_json} = assigns, socket) do
    changeset = Servers.change_assist_rule_json(assist_rules_json)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", params, socket) do
    changeset =
      socket.assigns.assist_rules_json
      |> Servers.change_assist_rule_json(params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", params, socket) do
    save_assist_rules_json(socket, socket.assigns.action, params)
  end

  defp save_assist_rules_json(socket, :edit, assist_rules_json_params) do
    case Servers.update_assist_rule_json(socket.assigns.assist_rules_json, assist_rules_json_params) do
      {:ok, assist_rules_json} ->
        notify_parent({:saved, assist_rules_json})

        {:noreply,
         socket
      }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_assist_rules_json(socket, :new, assist_rules_json_params) do
    case Servers.create_assist_rule_json(assist_rules_json_params) do
      {:ok, assist_rules_json} ->
        notify_parent({:saved, assist_rules_json})

        {:noreply,
         socket
      }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
