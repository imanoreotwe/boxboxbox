defmodule BoxboxboxWeb.EventRuleJSONLive.Show do
  use BoxboxboxWeb, :live_view

  alias Boxboxbox.Servers

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:event_rule_json, Servers.get_event_rule_json!(id))}
  end

  defp page_title(:show), do: "Show Event rule json"
  defp page_title(:edit), do: "Edit Event rule json"
end
