defmodule BoxboxboxWeb.EventRuleJSONLive.FormComponent do
  use BoxboxboxWeb, :live_component

  alias Boxboxbox.Servers

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage event_rule_json records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="event_rule_json-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:pitWindowLengthSec]} type="number" label="Pitwindowlengthsec" />
        <.input field={@form[:driverStintTimeSec]} type="number" label="Driverstinttimesec" />
        <.input field={@form[:mandatoryPitstopCount]} type="number" label="Mandatorypitstopcount" />
        <.input field={@form[:maxTotalDrivingTime]} type="number" label="Maxtotaldrivingtime" />
        <.input field={@form[:maxDriversCount]} type="number" label="Maxdriverscount" />
        <.input field={@form[:isRefuellingAllowedInRace]} type="checkbox" label="Isrefuellingallowedinrace" />
        <.input field={@form[:isRefuellingTimeFixed]} type="checkbox" label="Isrefuellingtimefixed" />
        <.input field={@form[:isMandatoryPitstopRefuellingRequired]} type="checkbox" label="Ismandatorypitstoprefuellingrequired" />
        <.input field={@form[:isMandatoryPitstopTyreChangeRequired]} type="checkbox" label="Ismandatorypitstoptyrechangerequired" />
        <.input field={@form[:isMandatoryPitstopSwapDriverRequired]} type="checkbox" label="Ismandatorypitstopswapdriverrequired" />
        <.input field={@form[:tyreSetCount]} type="number" label="Tyresetcount" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Event rule json</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{event_rule_json: event_rule_json} = assigns, socket) do
    changeset = Servers.change_event_rule_json(event_rule_json)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"event_rule_json" => event_rule_json_params}, socket) do
    changeset =
      socket.assigns.event_rule_json
      |> Servers.change_event_rule_json(event_rule_json_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"event_rule_json" => event_rule_json_params}, socket) do
    save_event_rule_json(socket, socket.assigns.action, event_rule_json_params)
  end

  defp save_event_rule_json(socket, :edit, event_rule_json_params) do
    case Servers.update_event_rule_json(socket.assigns.event_rule_json, event_rule_json_params) do
      {:ok, event_rule_json} ->
        notify_parent({:saved, event_rule_json})

        {:noreply,
         socket
      }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_event_rule_json(socket, :new, event_rule_json_params) do
    case Servers.create_event_rule_json(event_rule_json_params) do
      {:ok, event_rule_json} ->
        notify_parent({:saved, event_rule_json})

        {:noreply,
         socket
      }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
