defmodule BoxboxboxWeb.EventRuleJSONLive.Index do
  use BoxboxboxWeb, :live_view

  alias Boxboxbox.Servers
  alias Boxboxbox.Servers.EventRuleJSON

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :event_rule_jsons, Servers.list_event_rule_jsons())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Event rule json")
    |> assign(:event_rule_json, Servers.get_event_rule_json!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Event rule json")
    |> assign(:event_rule_json, %EventRuleJSON{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Event rule jsons")
    |> assign(:event_rule_json, nil)
  end

  @impl true
  def handle_info({BoxboxboxWeb.EventRuleJSONLive.FormComponent, {:saved, event_rule_json}}, socket) do
    {:noreply, stream_insert(socket, :event_rule_jsons, event_rule_json)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    event_rule_json = Servers.get_event_rule_json!(id)
    {:ok, _} = Servers.delete_event_rule_json(event_rule_json)

    {:noreply, stream_delete(socket, :event_rule_jsons, event_rule_json)}
  end
end
