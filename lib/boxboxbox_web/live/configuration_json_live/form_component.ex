defmodule BoxboxboxWeb.ConfigurationJSONLive.FormComponent do
  use BoxboxboxWeb, :live_component

  alias Boxboxbox.Servers

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage configuration_json records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="configuration_json-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:udpPort]} type="number" label="Udpport" />
        <.input field={@form[:tcpPort]} type="number" label="Tcpport" />
        <.input field={@form[:registerToLobby]} type="checkbox" label="Registertolobby" />
        <.input field={@form[:maxConnections]} type="number" label="Maxconnections" />
        <.input field={@form[:lanDiscovery]} type="checkbox" label="Landiscovery" />
        <.input field={@form[:publicIP]} type="text" label="Publicip" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Configuration json</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{configuration_json: configuration_json} = assigns, socket) do
    changeset = Servers.change_configuration_json(configuration_json)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"configuration_json" => configuration_json_params}, socket) do
    changeset =
      socket.assigns.configuration_json
      |> Servers.change_configuration_json(configuration_json_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"configuration_json" => configuration_json_params}, socket) do
    save_configuration_json(socket, socket.assigns.action, configuration_json_params)
  end

  defp save_configuration_json(socket, :edit, configuration_json_params) do
    case Servers.update_configuration_json(socket.assigns.configuration_json, configuration_json_params) do
      {:ok, configuration_json} ->
        notify_parent({:saved, configuration_json})

        {:noreply,
         socket
        }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_configuration_json(socket, :new, configuration_json_params) do
    case Servers.create_configuration_json(configuration_json_params) do
      {:ok, configuration_json} ->
        notify_parent({:saved, configuration_json})

        {:noreply,
         socket
        }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
