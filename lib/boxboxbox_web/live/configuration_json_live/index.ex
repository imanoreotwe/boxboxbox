defmodule BoxboxboxWeb.ConfigurationJSONLive.Index do
  use BoxboxboxWeb, :live_view

  alias Boxboxbox.Servers
  alias Boxboxbox.Servers.ConfigurationJSON

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :configuration_jsons, Servers.list_configuration_jsons())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Configuration json")
    |> assign(:configuration_json, Servers.get_configuration_json!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Configuration json")
    |> assign(:configuration_json, %ConfigurationJSON{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Configuration jsons")
    |> assign(:configuration_json, nil)
  end

  @impl true
  def handle_info({BoxboxboxWeb.ConfigurationJSONLive.FormComponent, {:saved, configuration_json}}, socket) do
    {:noreply, stream_insert(socket, :configuration_jsons, configuration_json)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    configuration_json = Servers.get_configuration_json!(id)
    {:ok, _} = Servers.delete_configuration_json(configuration_json)

    {:noreply, stream_delete(socket, :configuration_jsons, configuration_json)}
  end
end
