defmodule BoxboxboxWeb.SettingJSONLive.Index do
  use BoxboxboxWeb, :live_view

  alias Boxboxbox.Servers
  alias Boxboxbox.Servers.SettingJSON

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :setting_jsons, Servers.list_setting_jsons())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Setting json")
    |> assign(:setting_json, Servers.get_setting_json!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Setting json")
    |> assign(:setting_json, %SettingJSON{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Setting jsons")
    |> assign(:setting_json, nil)
  end

  @impl true
  def handle_info({BoxboxboxWeb.SettingJSONLive.FormComponent, {:saved, setting_json}}, socket) do
    {:noreply, stream_insert(socket, :setting_jsons, setting_json)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    setting_json = Servers.get_setting_json!(id)
    {:ok, _} = Servers.delete_setting_json(setting_json)

    {:noreply, stream_delete(socket, :setting_jsons, setting_json)}
  end
end
