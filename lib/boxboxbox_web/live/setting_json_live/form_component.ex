defmodule BoxboxboxWeb.SettingJSONLive.FormComponent do
  use BoxboxboxWeb, :live_component

  alias Boxboxbox.Servers

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage setting_json records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="setting_json-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:serverName]} type="text" label="Servername" />
        <.input field={@form[:adminPassword]} type="text" label="Adminpassword" />
        <.input field={@form[:trackMedalsRequirement]} type="number" label="Trackmedalsrequirement" />
        <.input field={@form[:safetyRatingRequirement]} type="number" label="Safetyratingrequirement" />
        <.input field={@form[:racecraftRatingRequirement]} type="number" label="Racecraftratingrequirement" />
        <.input field={@form[:password]} type="text" label="Password" />
        <.input field={@form[:spectatorPassword]} type="text" label="Spectatorpassword" />
        <.input field={@form[:maxCarSlots]} type="number" label="Maxcarslots" />
        <.input field={@form[:dumpLeaderboards]} type="checkbox" label="Dumpleaderboards" />
        <.input field={@form[:isRaceLocked]} type="checkbox" label="Isracelocked" />
        <.input field={@form[:randomizeTrackWhenEmpty]} type="checkbox" label="Randomizetrackwhenempty" />
        <.input field={@form[:centralEntryListPath]} type="text" label="Centralentrylistpath" />
        <.input field={@form[:allowAutoDQ]} type="checkbox" label="Allowautodq" />
        <.input field={@form[:shortFormationLap]} type="checkbox" label="Shortformationlap" />
        <.input field={@form[:dumpEntryList]} type="checkbox" label="Dumpentrylist" />
        <.input field={@form[:ignorePrematureDisconnects]} type="checkbox" label="Ignoreprematuredisconnects" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Setting json</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{setting_json: setting_json} = assigns, socket) do
    changeset = Servers.change_setting_json(setting_json)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"setting_json" => setting_json_params}, socket) do
    changeset =
      socket.assigns.setting_json
      |> Servers.change_setting_json(setting_json_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"setting_json" => setting_json_params}, socket) do
    save_setting_json(socket, socket.assigns.action, setting_json_params)
  end

  defp save_setting_json(socket, :edit, setting_json_params) do
    case Servers.update_setting_json(socket.assigns.setting_json, setting_json_params) do
      {:ok, setting_json} ->
        notify_parent({:saved, setting_json})

        {:noreply,
         socket}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_setting_json(socket, :new, setting_json_params) do
    case Servers.create_setting_json(setting_json_params) do
      {:ok, setting_json} ->
        notify_parent({:saved, setting_json})

        {:noreply,
         socket}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
