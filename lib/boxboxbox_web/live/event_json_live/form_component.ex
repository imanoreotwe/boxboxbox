defmodule BoxboxboxWeb.EventJSONLive.FormComponent do
  use BoxboxboxWeb, :live_component

  alias Boxboxbox.Servers

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage event_json records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="event_json-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:preRaceWaitingTimeSeconds]} type="number" label="Preracewaitingtimeseconds" />
        <.input field={@form[:sessionOverTimeSeconds]} type="number" label="Sessionovertimeseconds" />
        <.input field={@form[:ambiantTemp]} type="number" label="Ambianttemp" />
        <.input field={@form[:cloudLevel]} type="number" label="Cloudlevel" step="any" />
        <.input field={@form[:rain]} type="number" label="Rain" step="any" />
        <.input field={@form[:weatherRandomness]} type="number" label="Weatherrandomness" />
        <.input field={@form[:postQualySeconds]} type="number" label="Postqualyseconds" />
        <.input field={@form[:postRaceSeconds]} type="number" label="Postraceseconds" />
        <.input field={@form[:metaData]} type="text" label="Metadata" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Event json</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{event_json: event_json} = assigns, socket) do
    changeset = Servers.change_event_json(event_json)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"event_json" => event_json_params}, socket) do
    changeset =
      socket.assigns.event_json
      |> Servers.change_event_json(event_json_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"event_json" => event_json_params}, socket) do
    save_event_json(socket, socket.assigns.action, event_json_params)
  end

  defp save_event_json(socket, :edit, event_json_params) do
    case Servers.update_event_json(socket.assigns.event_json, event_json_params) do
      {:ok, event_json} ->
        notify_parent({:saved, event_json})

        {:noreply,
         socket
      }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_event_json(socket, :new, event_json_params) do
    case Servers.create_event_json(event_json_params) do
      {:ok, event_json} ->
        notify_parent({:saved, event_json})

        {:noreply,
         socket
      }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
