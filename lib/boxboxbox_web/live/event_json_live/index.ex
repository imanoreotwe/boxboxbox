defmodule BoxboxboxWeb.EventJSONLive.Index do
  use BoxboxboxWeb, :live_view

  alias Boxboxbox.Servers
  alias Boxboxbox.Servers.EventJSON

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :event_jsons, Servers.list_event_jsons())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Event json")
    |> assign(:event_json, Servers.get_event_json!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Event json")
    |> assign(:event_json, %EventJSON{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Event jsons")
    |> assign(:event_json, nil)
  end

  @impl true
  def handle_info({BoxboxboxWeb.EventJSONLive.FormComponent, {:saved, event_json}}, socket) do
    {:noreply, stream_insert(socket, :event_jsons, event_json)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    event_json = Servers.get_event_json!(id)
    {:ok, _} = Servers.delete_event_json(event_json)

    {:noreply, stream_delete(socket, :event_jsons, event_json)}
  end
end
