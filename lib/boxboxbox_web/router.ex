defmodule BoxboxboxWeb.Router do
  use BoxboxboxWeb, :router

  import BoxboxboxWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {BoxboxboxWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BoxboxboxWeb do
    pipe_through :browser

    get "/", PageController, :home
  end

  # Other scopes may use custom stacks.
  # scope "/api", BoxboxboxWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:boxboxbox, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: BoxboxboxWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", BoxboxboxWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [{BoxboxboxWeb.UserAuth, :redirect_if_user_is_authenticated}] do
      live "/users/register", UserRegistrationLive, :new
      live "/users/log_in", UserLoginLive, :new
      live "/users/reset_password", UserForgotPasswordLive, :new
      live "/users/reset_password/:token", UserResetPasswordLive, :edit
    end

    post "/users/log_in", UserSessionController, :create
  end

  scope "/", BoxboxboxWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :require_authenticated_user,
      on_mount: [{BoxboxboxWeb.UserAuth, :ensure_authenticated}] do
      live "/users/settings", UserSettingsLive, :edit
      live "/users/settings/confirm_email/:token", UserSettingsLive, :confirm_email

      live "/servers", ServerLive.Index, :index
      live "/servers/new", ServerLive.Index, :new
      live "/servers/:id/edit", ServerLive.Index, :edit

      live "/servers/:id", ServerLive.Show, :show
      live "/servers/:id/show/edit", ServerLive.Show, :edit
    end
  end

  scope "/", BoxboxboxWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete

    live_session :current_user,
      on_mount: [{BoxboxboxWeb.UserAuth, :mount_current_user}] do
      live "/users/confirm/:token", UserConfirmationLive, :edit
      live "/users/confirm", UserConfirmationInstructionsLive, :new
    end
  end
end

"""
live "/configuration_jsons", ConfigurationJSONLive.Index, :index
live "/configuration_jsons/new", ConfigurationJSONLive.Index, :new
live "/configuration_jsons/:id/edit", ConfigurationJSONLive.Index, :edit

live "/configuration_jsons/:id", ConfigurationJSONLive.Show, :show
live "/configuration_jsons/:id/show/edit", ConfigurationJSONLive.Show, :edit

live "/event_jsons", EventJSONLive.Index, :index
live "/event_jsons/new", EventJSONLive.Index, :new
live "/event_jsons/:id/edit", EventJSONLive.Index, :edit

live "/event_jsons/:id", EventJSONLive.Show, :show
live "/event_jsons/:id/show/edit", EventJSONLive.Show, :edi

live "/tracks", TrackLive.Index, :index
live "/tracks/new", TrackLive.Index, :new
live "/tracks/:id/edit", TrackLive.Index, :edit

live "/tracks/:id", TrackLive.Show, :show
live "/tracks/:id/show/edit", TrackLive.Show, :edit

live "/setting_jsons", SettingJSONLive.Index, :index
live "/setting_jsons/new", SettingJSONLive.Index, :new
live "/setting_jsons/:id/edit", SettingJSONLive.Index, :edit

live "/setting_jsons/:id", SettingJSONLive.Show, :show
live "/setting_jsons/:id/show/edit", SettingJSONLive.Show, :edit

live "/event_rule_jsons", EventRuleJSONLive.Index, :index
live "/event_rule_jsons/new", EventRuleJSONLive.Index, :new
live "/event_rule_jsons/:id/edit", EventRuleJSONLive.Index, :edit

live "/event_rule_jsons/:id", EventRuleJSONLive.Show, :show
live "/event_rule_jsons/:id/show/edit", EventRuleJSONLive.Show, :edit

live "/assist_rule_jsons", AssistRulesJSONLive.Index, :index
live "/assist_rule_jsons/new", AssistRulesJSONLive.Index, :new
live "/assist_rule_jsons/:id/edit", AssistRulesJSONLive.Index, :edit

live "/assist_rule_jsons/:id", AssistRulesJSONLive.Show, :show
live "/assist_rule_jsons/:id/show/edit", AssistRulesJSONLive.Show, :edit

"""
