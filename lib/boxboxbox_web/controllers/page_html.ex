defmodule BoxboxboxWeb.PageHTML do
  use BoxboxboxWeb, :html

  embed_templates "page_html/*"
end
