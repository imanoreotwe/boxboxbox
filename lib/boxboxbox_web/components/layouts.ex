defmodule BoxboxboxWeb.Layouts do
  use BoxboxboxWeb, :html

  embed_templates "layouts/*"
end
