defmodule Boxboxbox.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      BoxboxboxWeb.Telemetry,
      Boxboxbox.Repo,
      {DNSCluster, query: Application.get_env(:boxboxbox, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Boxboxbox.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: Boxboxbox.Finch},
      # Supervisor for docker containers
      {Boxboxbox.DockerSupervisor, name: Boxboxbox.DockerSupervisor},
      # Start to serve requests, typically the last entry
      BoxboxboxWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Boxboxbox.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    BoxboxboxWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
