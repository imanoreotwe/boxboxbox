defmodule Boxboxbox.Cars do
  @moduledoc """
  The Cars context.
  """

  import Ecto.Query, warn: false
  alias Boxboxbox.Repo

  alias Boxboxbox.Cars.CarGroup

  @doc """
  Returns the list of car_groups.

  ## Examples

      iex> list_car_groups()
      [%CarGroup{}, ...]

  """
  def list_car_groups do
    Repo.all(CarGroup)
  end

  @doc """
  Gets a single car_group.

  Raises `Ecto.NoResultsError` if the Car group does not exist.

  ## Examples

      iex> get_car_group!(123)
      %CarGroup{}

      iex> get_car_group!(456)
      ** (Ecto.NoResultsError)

  """
  def get_car_group!(id), do: Repo.get!(CarGroup, id)

  @doc """
  Creates a car_group.

  ## Examples

      iex> create_car_group(%{field: value})
      {:ok, %CarGroup{}}

      iex> create_car_group(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_car_group(attrs \\ %{}) do
    %CarGroup{}
    |> CarGroup.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a car_group.

  ## Examples

      iex> update_car_group(car_group, %{field: new_value})
      {:ok, %CarGroup{}}

      iex> update_car_group(car_group, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_car_group(%CarGroup{} = car_group, attrs) do
    car_group
    |> CarGroup.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a car_group.

  ## Examples

      iex> delete_car_group(car_group)
      {:ok, %CarGroup{}}

      iex> delete_car_group(car_group)
      {:error, %Ecto.Changeset{}}

  """
  def delete_car_group(%CarGroup{} = car_group) do
    Repo.delete(car_group)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking car_group changes.

  ## Examples

      iex> change_car_group(car_group)
      %Ecto.Changeset{data: %CarGroup{}}

  """
  def change_car_group(%CarGroup{} = car_group, attrs \\ %{}) do
    CarGroup.changeset(car_group, attrs)
  end
end
