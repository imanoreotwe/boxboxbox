defmodule Boxboxbox.Servers do
  @moduledoc """
  The Servers context.
  """

  import Ecto.Query, warn: false
  alias Boxboxbox.Repo

  alias Boxboxbox.Servers.FormationLapType

  @doc """
  Returns the list of formation_lap_types.

  ## Examples

      iex> list_formation_lap_types()
      [%FormationLapType{}, ...]

  """
  def list_formation_lap_types do
    Repo.all(FormationLapType)
  end

  @doc """
  Gets a single formation_lap_type.

  Raises `Ecto.NoResultsError` if the Formation lap type does not exist.

  ## Examples

      iex> get_formation_lap_type!(123)
      %FormationLapType{}

      iex> get_formation_lap_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_formation_lap_type!(id), do: Repo.get!(FormationLapType, id)

  @doc """
  Creates a formation_lap_type.

  ## Examples

      iex> create_formation_lap_type(%{field: value})
      {:ok, %FormationLapType{}}

      iex> create_formation_lap_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_formation_lap_type(attrs \\ %{}) do
    %FormationLapType{}
    |> FormationLapType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a formation_lap_type.

  ## Examples

      iex> update_formation_lap_type(formation_lap_type, %{field: new_value})
      {:ok, %FormationLapType{}}

      iex> update_formation_lap_type(formation_lap_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_formation_lap_type(%FormationLapType{} = formation_lap_type, attrs) do
    formation_lap_type
    |> FormationLapType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a formation_lap_type.

  ## Examples

      iex> delete_formation_lap_type(formation_lap_type)
      {:ok, %FormationLapType{}}

      iex> delete_formation_lap_type(formation_lap_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_formation_lap_type(%FormationLapType{} = formation_lap_type) do
    Repo.delete(formation_lap_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking formation_lap_type changes.

  ## Examples

      iex> change_formation_lap_type(formation_lap_type)
      %Ecto.Changeset{data: %FormationLapType{}}

  """
  def change_formation_lap_type(%FormationLapType{} = formation_lap_type, attrs \\ %{}) do
    FormationLapType.changeset(formation_lap_type, attrs)
  end

  alias Boxboxbox.Servers.ConfigurationJSON

  @doc """
  Returns the list of configuration_jsons.

  ## Examples

      iex> list_configuration_jsons()
      [%ConfigurationJSON{}, ...]

  """
  def list_configuration_jsons do
    Repo.all(ConfigurationJSON)
  end

  @doc """
  Gets a single configuration_json.

  Raises `Ecto.NoResultsError` if the Configuration json does not exist.

  ## Examples

      iex> get_configuration_json!(123)
      %ConfigurationJSON{}

      iex> get_configuration_json!(456)
      ** (Ecto.NoResultsError)

  """
  def get_configuration_json!(id), do: Repo.get!(ConfigurationJSON, id)

  @doc """
  Creates a configuration_json.

  ## Examples

      iex> create_configuration_json(%{field: value})
      {:ok, %ConfigurationJSON{}}

      iex> create_configuration_json(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_configuration_json(attrs \\ %{}) do
    %ConfigurationJSON{}
    |> ConfigurationJSON.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a configuration_json.

  ## Examples

      iex> update_configuration_json(configuration_json, %{field: new_value})
      {:ok, %ConfigurationJSON{}}

      iex> update_configuration_json(configuration_json, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_configuration_json(%ConfigurationJSON{} = configuration_json, attrs) do
    configuration_json
    |> ConfigurationJSON.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a configuration_json.

  ## Examples

      iex> delete_configuration_json(configuration_json)
      {:ok, %ConfigurationJSON{}}

      iex> delete_configuration_json(configuration_json)
      {:error, %Ecto.Changeset{}}

  """
  def delete_configuration_json(%ConfigurationJSON{} = configuration_json) do
    Repo.delete(configuration_json)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking configuration_json changes.

  ## Examples

      iex> change_configuration_json(configuration_json)
      %Ecto.Changeset{data: %ConfigurationJSON{}}

  """
  def change_configuration_json(%ConfigurationJSON{} = configuration_json, attrs \\ %{}) do
    ConfigurationJSON.changeset(configuration_json, attrs)
  end

  alias Boxboxbox.Servers.Server

  @doc """
  Returns the list of servers.

  ## Examples

      iex> list_servers()
      [%Server{}, ...]

  """
  def list_servers do
    Repo.all(Server)
  end

  @doc """
  Gets a single server.

  Raises `Ecto.NoResultsError` if the Server does not exist.

  ## Examples

      iex> get_server!(123)
      %Server{}

      iex> get_server!(456)
      ** (Ecto.NoResultsError)

  """
  def get_server!(id), do: Repo.get!(Server, id)

  @doc """
  Creates a server.

  ## Examples

      iex> create_server(%{field: value})
      {:ok, %Server{}}

      iex> create_server(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_server(attrs \\ %{}) do
    %Server{}
    |> Server.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a server.

  ## Examples

      iex> update_server(server, %{field: new_value})
      {:ok, %Server{}}

      iex> update_server(server, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_server(%Server{} = server, attrs) do
    server
    |> Server.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a server.

  ## Examples

      iex> delete_server(server)
      {:ok, %Server{}}

      iex> delete_server(server)
      {:error, %Ecto.Changeset{}}

  """
  def delete_server(%Server{} = server) do
    Repo.delete(server)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking server changes.

  ## Examples

      iex> change_server(server)
      %Ecto.Changeset{data: %Server{}}

  """
  def change_server(%Server{} = server, attrs \\ %{}) do
    Server.changeset(server, attrs)
  end

  alias Boxboxbox.Servers.SettingJSON

  @doc """
  Returns the list of setting_jsons.

  ## Examples

      iex> list_setting_jsons()
      [%SettingJSON{}, ...]

  """
  def list_setting_jsons do
    Repo.all(SettingJSON)
  end

  @doc """
  Gets a single setting_json.

  Raises `Ecto.NoResultsError` if the Setting json does not exist.

  ## Examples

      iex> get_setting_json!(123)
      %SettingJSON{}

      iex> get_setting_json!(456)
      ** (Ecto.NoResultsError)

  """
  def get_setting_json!(id), do: Repo.get!(SettingJSON, id)

  @doc """
  Creates a setting_json.

  ## Examples

      iex> create_setting_json(%{field: value})
      {:ok, %SettingJSON{}}

      iex> create_setting_json(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_setting_json(attrs \\ %{}) do
    %SettingJSON{}
    |> SettingJSON.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a setting_json.

  ## Examples

      iex> update_setting_json(setting_json, %{field: new_value})
      {:ok, %SettingJSON{}}

      iex> update_setting_json(setting_json, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_setting_json(%SettingJSON{} = setting_json, attrs) do
    setting_json
    |> SettingJSON.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a setting_json.

  ## Examples

      iex> delete_setting_json(setting_json)
      {:ok, %SettingJSON{}}

      iex> delete_setting_json(setting_json)
      {:error, %Ecto.Changeset{}}

  """
  def delete_setting_json(%SettingJSON{} = setting_json) do
    Repo.delete(setting_json)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking setting_json changes.

  ## Examples

      iex> change_setting_json(setting_json)
      %Ecto.Changeset{data: %SettingJSON{}}

  """
  def change_setting_json(%SettingJSON{} = setting_json, attrs \\ %{}) do
    SettingJSON.changeset(setting_json, attrs)
  end

  alias Boxboxbox.Servers.SessionType

  @doc """
  Returns the list of session_types.

  ## Examples

      iex> list_session_types()
      [%SessionType{}, ...]

  """
  def list_session_types do
    Repo.all(SessionType)
  end

  @doc """
  Gets a single session_type.

  Raises `Ecto.NoResultsError` if the Session type does not exist.

  ## Examples

      iex> get_session_type!(123)
      %SessionType{}

      iex> get_session_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_session_type!(id), do: Repo.get!(SessionType, id)

  @doc """
  Creates a session_type.

  ## Examples

      iex> create_session_type(%{field: value})
      {:ok, %SessionType{}}

      iex> create_session_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_session_type(attrs \\ %{}) do
    %SessionType{}
    |> SessionType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a session_type.

  ## Examples

      iex> update_session_type(session_type, %{field: new_value})
      {:ok, %SessionType{}}

      iex> update_session_type(session_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_session_type(%SessionType{} = session_type, attrs) do
    session_type
    |> SessionType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a session_type.

  ## Examples

      iex> delete_session_type(session_type)
      {:ok, %SessionType{}}

      iex> delete_session_type(session_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_session_type(%SessionType{} = session_type) do
    Repo.delete(session_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking session_type changes.

  ## Examples

      iex> change_session_type(session_type)
      %Ecto.Changeset{data: %SessionType{}}

  """
  def change_session_type(%SessionType{} = session_type, attrs \\ %{}) do
    SessionType.changeset(session_type, attrs)
  end

  alias Boxboxbox.Servers.Session

  @doc """
  Returns the list of sessions.

  ## Examples

      iex> list_sessions()
      [%Session{}, ...]

  """
  def list_sessions do
    Repo.all(Session)
  end

  @doc """
  Gets a single session.

  Raises `Ecto.NoResultsError` if the Session does not exist.

  ## Examples

      iex> get_session!(123)
      %Session{}

      iex> get_session!(456)
      ** (Ecto.NoResultsError)

  """
  def get_session!(id), do: Repo.get!(Session, id)

  @doc """
  Creates a session.

  ## Examples

      iex> create_session(%{field: value})
      {:ok, %Session{}}

      iex> create_session(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_session(attrs \\ %{}) do
    %Session{}
    |> Session.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a session.

  ## Examples

      iex> update_session(session, %{field: new_value})
      {:ok, %Session{}}

      iex> update_session(session, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_session(%Session{} = session, attrs) do
    session
    |> Session.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a session.

  ## Examples

      iex> delete_session(session)
      {:ok, %Session{}}

      iex> delete_session(session)
      {:error, %Ecto.Changeset{}}

  """
  def delete_session(%Session{} = session) do
    Repo.delete(session)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking session changes.

  ## Examples

      iex> change_session(session)
      %Ecto.Changeset{data: %Session{}}

  """
  def change_session(%Session{} = session, attrs \\ %{}) do
    Session.changeset(session, attrs)
  end

  alias Boxboxbox.Servers.EventJSON

  @doc """
  Returns the list of event_jsons.

  ## Examples

      iex> list_event_jsons()
      [%EventJSON{}, ...]

  """
  def list_event_jsons do
    Repo.all(EventJSON)
  end

  @doc """
  Gets a single event_json.

  Raises `Ecto.NoResultsError` if the Event json does not exist.

  ## Examples

      iex> get_event_json!(123)
      %EventJSON{}

      iex> get_event_json!(456)
      ** (Ecto.NoResultsError)

  """
  def get_event_json!(id), do: Repo.get!(EventJSON, id)

  @doc """
  Creates a event_json.

  ## Examples

      iex> create_event_json(%{field: value})
      {:ok, %EventJSON{}}

      iex> create_event_json(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_event_json(attrs \\ %{}) do
    %EventJSON{}
    |> EventJSON.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a event_json.

  ## Examples

      iex> update_event_json(event_json, %{field: new_value})
      {:ok, %EventJSON{}}

      iex> update_event_json(event_json, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_event_json(%EventJSON{} = event_json, attrs) do
    event_json
    |> EventJSON.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a event_json.

  ## Examples

      iex> delete_event_json(event_json)
      {:ok, %EventJSON{}}

      iex> delete_event_json(event_json)
      {:error, %Ecto.Changeset{}}

  """
  def delete_event_json(%EventJSON{} = event_json) do
    Repo.delete(event_json)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking event_json changes.

  ## Examples

      iex> change_event_json(event_json)
      %Ecto.Changeset{data: %EventJSON{}}

  """
  def change_event_json(%EventJSON{} = event_json, attrs \\ %{}) do
    EventJSON.changeset(event_json, attrs)
  end

  alias Boxboxbox.Servers.QualifyType

  @doc """
  Returns the list of qualify_types.

  ## Examples

      iex> list_qualify_types()
      [%QualifyType{}, ...]

  """
  def list_qualify_types do
    Repo.all(QualifyType)
  end

  @doc """
  Gets a single qualify_type.

  Raises `Ecto.NoResultsError` if the Qualify type does not exist.

  ## Examples

      iex> get_qualify_type!(123)
      %QualifyType{}

      iex> get_qualify_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_qualify_type!(id), do: Repo.get!(QualifyType, id)

  @doc """
  Creates a qualify_type.

  ## Examples

      iex> create_qualify_type(%{field: value})
      {:ok, %QualifyType{}}

      iex> create_qualify_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_qualify_type(attrs \\ %{}) do
    %QualifyType{}
    |> QualifyType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a qualify_type.

  ## Examples

      iex> update_qualify_type(qualify_type, %{field: new_value})
      {:ok, %QualifyType{}}

      iex> update_qualify_type(qualify_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_qualify_type(%QualifyType{} = qualify_type, attrs) do
    qualify_type
    |> QualifyType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a qualify_type.

  ## Examples

      iex> delete_qualify_type(qualify_type)
      {:ok, %QualifyType{}}

      iex> delete_qualify_type(qualify_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_qualify_type(%QualifyType{} = qualify_type) do
    Repo.delete(qualify_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking qualify_type changes.

  ## Examples

      iex> change_qualify_type(qualify_type)
      %Ecto.Changeset{data: %QualifyType{}}

  """
  def change_qualify_type(%QualifyType{} = qualify_type, attrs \\ %{}) do
    QualifyType.changeset(qualify_type, attrs)
  end

  alias Boxboxbox.Servers.EventRuleJSON

  @doc """
  Returns the list of event_rule_jsons.

  ## Examples

      iex> list_event_rule_jsons()
      [%EventRuleJSON{}, ...]

  """
  def list_event_rule_jsons do
    Repo.all(EventRuleJSON)
  end

  @doc """
  Gets a single event_rule_json.

  Raises `Ecto.NoResultsError` if the Event rule json does not exist.

  ## Examples

      iex> get_event_rule_json!(123)
      %EventRuleJSON{}

      iex> get_event_rule_json!(456)
      ** (Ecto.NoResultsError)

  """
  def get_event_rule_json!(id), do: Repo.get!(EventRuleJSON, id)

  @doc """
  Creates a event_rule_json.

  ## Examples

      iex> create_event_rule_json(%{field: value})
      {:ok, %EventRuleJSON{}}

      iex> create_event_rule_json(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_event_rule_json(attrs \\ %{}) do
    %EventRuleJSON{}
    |> EventRuleJSON.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a event_rule_json.

  ## Examples

      iex> update_event_rule_json(event_rule_json, %{field: new_value})
      {:ok, %EventRuleJSON{}}

      iex> update_event_rule_json(event_rule_json, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_event_rule_json(%EventRuleJSON{} = event_rule_json, attrs) do
    event_rule_json
    |> EventRuleJSON.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a event_rule_json.

  ## Examples

      iex> delete_event_rule_json(event_rule_json)
      {:ok, %EventRuleJSON{}}

      iex> delete_event_rule_json(event_rule_json)
      {:error, %Ecto.Changeset{}}

  """
  def delete_event_rule_json(%EventRuleJSON{} = event_rule_json) do
    Repo.delete(event_rule_json)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking event_rule_json changes.

  ## Examples

      iex> change_event_rule_json(event_rule_json)
      %Ecto.Changeset{data: %EventRuleJSON{}}

  """
  def change_event_rule_json(%EventRuleJSON{} = event_rule_json, attrs \\ %{}) do
    EventRuleJSON.changeset(event_rule_json, attrs)
  end

  alias Boxboxbox.Servers.AssistRuleJSON

  @doc """
  Returns the list of assist_rule_jsons.

  ## Examples

      iex> list_assist_rule_jsons()
      [%AssistRuleJSON{}, ...]

  """
  def list_assist_rule_jsons do
    Repo.all(AssistRuleJSON)
  end

  @doc """
  Gets a single assist_rules_json.

  Raises `Ecto.NoResultsError` if the Assist rules json does not exist.

  ## Examples

      iex> get_assist_rules_json!(123)
      %AssistRuleJSON{}

      iex> get_assist_rules_json!(456)
      ** (Ecto.NoResultsError)

  """
  def get_assist_rule_json!(id), do: Repo.get!(AssistRuleJSON, id)

  @doc """
  Creates a assist_rules_json.

  ## Examples

      iex> create_assist_rules_json(%{field: value})
      {:ok, %AssistRuleJSON{}}

      iex> create_assist_rules_json(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_assist_rule_json(attrs \\ %{}) do
    %AssistRuleJSON{}
    |> AssistRuleJSON.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a assist_rules_json.

  ## Examples

      iex> update_assist_rules_json(assist_rules_json, %{field: new_value})
      {:ok, %AssistRuleJSON{}}

      iex> update_assist_rules_json(assist_rules_json, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_assist_rule_json(%AssistRuleJSON{} = assist_rules_json, attrs) do
    assist_rules_json
    |> AssistRuleJSON.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a assist_rules_json.

  ## Examples

      iex> delete_assist_rules_json(assist_rules_json)
      {:ok, %AssistRuleJSON{}}

      iex> delete_assist_rules_json(assist_rules_json)
      {:error, %Ecto.Changeset{}}

  """
  def delete_assist_rule_json(%AssistRuleJSON{} = assist_rules_json) do
    Repo.delete(assist_rules_json)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking assist_rules_json changes.

  ## Examples

      iex> change_assist_rules_json(assist_rules_json)
      %Ecto.Changeset{data: %AssistRuleJSON{}}

  """
  def change_assist_rule_json(%AssistRuleJSON{} = assist_rules_json, attrs \\ %{}) do
    AssistRuleJSON.changeset(assist_rules_json, attrs)
  end


  @doc "generates json files for all server configs. Requires `dir` to be a writable directory."
  def generate_config_files(%Server{} = server, dir) do
    config_dir = Path.join(dir, "configuration.json")
    get_configuration_json!(server.configuration) |> encode_and_save!(config_dir)

    setting_dir = Path.join(dir, "settings.json")
    get_setting_json!(server.setting) |> encode_and_save!(setting_dir)

    event_dir = Path.join(dir, "event.json")
    get_event_json!(server.event) |> encode_and_save!(event_dir)

    assist_rule_dir = Path.join(dir, "assistRules.json")
    get_event_json!(server.assist_rule) |> encode_and_save!(assist_rule_dir)
  end

  defp encode_and_save!(json, file) do
    json_string = Jason.encode!(json)
    File.write!(file, json_string)
  end
end
