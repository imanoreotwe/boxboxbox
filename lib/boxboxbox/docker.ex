defmodule Boxboxbox.Docker do
  @moduledoc """
  Docker API module
  """

  @socket_path "http+unix://#{URI.encode_www_form("/var/run/docker.sock")}/v1.45"

  use Tesla

  plug Tesla.Middleware.BaseUrl, @socket_path
  plug Tesla.Middleware.Logger

  def build(tar) do
    stream = "{% raw %}"
    |> Stream.concat(File.stream!(tar))
    |> Stream.concat("{% endraw %}")

    resp = post("/build", stream, headers: [{"Content-Type", "application/x-tar"}])
    IO.inspect(resp)
  end
end
