defmodule Boxboxbox.Servers.QualifyType do
  use Ecto.Schema
  import Ecto.Changeset

  schema "qualify_types" do
    field :enabled, :boolean, default: false
    field :name, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(qualify_type, attrs) do
    qualify_type
    |> cast(attrs, [:name, :enabled])
    |> validate_required([:name, :enabled])
  end
end
