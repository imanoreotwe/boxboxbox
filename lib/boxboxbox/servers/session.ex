defmodule Boxboxbox.Servers.Session do
  use Ecto.Schema
  import Ecto.Changeset

  schema "sessions" do
    field :hourOfDay, :integer
    field :dayOfWeekend, :integer
    field :timeMultiplier, :integer
    field :sessionDurationMinutes, :integer
    field :sessionType, :id
    field :event_jsons, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(session, attrs) do
    session
    |> cast(attrs, [:hourOfDay, :dayOfWeekend, :timeMultiplier, :sessionDurationMinutes])
    |> validate_required([:hourOfDay, :dayOfWeekend, :timeMultiplier, :sessionDurationMinutes])
  end
end
