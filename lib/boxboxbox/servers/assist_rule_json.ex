defmodule Boxboxbox.Servers.AssistRuleJSON do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:stabilityControlLevelMax, :disableAutosteer, :disableAutoLights, :disableAutoWiper, :disableAutoEngineStart, :disableAutoPitLimiter, :disableAutoClutch, :disableAutoGear, :disableIdealLine]}
  schema "assist_rule_jsons" do
    field :stabilityControlLevelMax, :integer
    field :disableAutosteer, :boolean, default: false
    field :disableAutoLights, :boolean, default: false
    field :disableAutoWiper, :boolean, default: false
    field :disableAutoEngineStart, :boolean, default: false
    field :disableAutoPitLimiter, :boolean, default: false
    field :disableAutoClutch, :boolean, default: false
    field :disableAutoGear, :boolean, default: false
    field :disableIdealLine, :boolean, default: false

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(assist_rule_json, attrs) do
    assist_rule_json
    |> cast(attrs, [:stabilityControlLevelMax, :disableAutosteer, :disableAutoLights, :disableAutoWiper, :disableAutoEngineStart, :disableAutoPitLimiter, :disableAutoClutch, :disableAutoGear, :disableIdealLine])
    |> validate_required([])
  end
end
