defmodule Boxboxbox.Servers.SettingJSON do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:serverName, :adminPassword, :trackMedalsRequirement, :safetyRatingRequirement, :racecraftRatingRequirement, :password, :spectatorPassword, :maxCarSlots, :dumpLeaderboards, :isRaceLocked, :randomizeTrackWhenEmpty, :centralEntryListPath, :allowAutoDQ, :shortFormationLap, :dumpEntryList, :ignorePrematureDisconnects]}
  schema "setting_jsons" do
    field :password, :string
    field :serverName, :string
    field :adminPassword, :string
    field :trackMedalsRequirement, :integer
    field :safetyRatingRequirement, :integer
    field :racecraftRatingRequirement, :integer
    field :spectatorPassword, :string
    field :maxCarSlots, :integer
    field :dumpLeaderboards, :boolean, default: true
    field :isRaceLocked, :boolean, default: true
    field :randomizeTrackWhenEmpty, :boolean, default: false
    field :centralEntryListPath, :string
    field :allowAutoDQ, :boolean, default: true
    field :shortFormationLap, :boolean, default: true
    field :dumpEntryList, :boolean, default: false
    field :ignorePrematureDisconnects, :boolean, default: false
    field :carGroup, :id
    field :formationLapType, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(setting_json, attrs) do
    setting_json
    |> cast(attrs, [:serverName, :adminPassword, :trackMedalsRequirement, :safetyRatingRequirement, :racecraftRatingRequirement, :password, :spectatorPassword, :maxCarSlots, :dumpLeaderboards, :isRaceLocked, :randomizeTrackWhenEmpty, :centralEntryListPath, :allowAutoDQ, :shortFormationLap, :dumpEntryList, :ignorePrematureDisconnects])
    |> validate_required([:serverName, :ignorePrematureDisconnects])
  end
end
