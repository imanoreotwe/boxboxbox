defmodule Boxboxbox.Servers.ConfigurationJSON do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:udpPort, :tcpPort, :registerToLobby, :maxConnections, :lanDiscovery, :publicIP]}
  schema "configuration_jsons" do
    field :udpPort, :integer
    field :tcpPort, :integer
    field :registerToLobby, :boolean, default: true
    field :maxConnections, :integer
    field :lanDiscovery, :boolean, default: false
    field :publicIP, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(configuration_json, attrs) do
    configuration_json
    |> cast(attrs, [:udpPort, :tcpPort, :registerToLobby, :maxConnections, :lanDiscovery, :publicIP])
    |> validate_required([:udpPort, :tcpPort, :registerToLobby, :maxConnections, :lanDiscovery, :publicIP])
  end
end
