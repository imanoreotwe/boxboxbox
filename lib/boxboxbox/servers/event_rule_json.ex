defmodule Boxboxbox.Servers.EventRuleJSON do
  use Ecto.Schema
  import Ecto.Changeset

  schema "event_rule_jsons" do
    field :pitWindowLengthSec, :integer
    field :driverStintTimeSec, :integer
    field :mandatoryPitstopCount, :integer
    field :maxTotalDrivingTime, :integer
    field :maxDriversCount, :integer
    field :isRefuellingAllowedInRace, :boolean, default: false
    field :isRefuellingTimeFixed, :boolean, default: false
    field :isMandatoryPitstopRefuellingRequired, :boolean, default: false
    field :isMandatoryPitstopTyreChangeRequired, :boolean, default: false
    field :isMandatoryPitstopSwapDriverRequired, :boolean, default: false
    field :tyreSetCount, :integer
    field :qualifyStandingType, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(event_rule_json, attrs) do
    event_rule_json
    |> cast(attrs, [:pitWindowLengthSec, :driverStintTimeSec, :mandatoryPitstopCount, :maxTotalDrivingTime, :maxDriversCount, :isRefuellingAllowedInRace, :isRefuellingTimeFixed, :isMandatoryPitstopRefuellingRequired, :isMandatoryPitstopTyreChangeRequired, :isMandatoryPitstopSwapDriverRequired, :tyreSetCount])
    |> validate_required([])
  end
end
