defmodule Boxboxbox.Servers.Server do
  use Ecto.Schema
  import Ecto.Changeset

  schema "servers" do

    field :owner, :id
    field :configuration, :id
    field :setting, :id
    field :event, :id
    field :event_rule, :id
    field :assist_rule, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(server, attrs) do
    server
    |> cast(attrs, [:owner, :configuration, :setting, :event, :event_rule, :assist_rule])
    |> validate_required([:owner, :configuration, :setting, :event, :event_rule, :assist_rule])
  end
end
