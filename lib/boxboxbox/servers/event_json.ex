defmodule Boxboxbox.Servers.EventJSON do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:preRaceWaitingTimeSeconds, :sessionOverTimeSeconds, :ambiantTemp, :cloudLevel, :rain, :weatherRandomness, :postQualySeconds, :postRaceSeconds, :metaData, :track]}
  schema "event_jsons" do
    field :preRaceWaitingTimeSeconds, :integer
    field :sessionOverTimeSeconds, :integer
    field :ambiantTemp, :integer
    field :cloudLevel, :float
    field :rain, :float
    field :weatherRandomness, :integer
    field :postQualySeconds, :integer
    field :postRaceSeconds, :integer
    field :metaData, :string
    field :track, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(event_json, attrs) do
    event_json
    |> cast(attrs, [:preRaceWaitingTimeSeconds, :sessionOverTimeSeconds, :ambiantTemp, :cloudLevel, :rain, :weatherRandomness, :postQualySeconds, :postRaceSeconds, :metaData])
    |> validate_required([])
  end
end
