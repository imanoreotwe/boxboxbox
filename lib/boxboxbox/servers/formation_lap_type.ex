defmodule Boxboxbox.Servers.FormationLapType do
  use Ecto.Schema
  import Ecto.Changeset

  schema "formation_lap_types" do
    field :name, :string
    field :value, :integer
    field :description, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(formation_lap_type, attrs) do
    formation_lap_type
    |> cast(attrs, [:name, :description, :value])
    |> validate_required([:name, :description, :value])
  end
end
