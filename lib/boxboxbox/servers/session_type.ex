defmodule Boxboxbox.Servers.SessionType do
  use Ecto.Schema
  import Ecto.Changeset

  schema "session_types" do
    field :name, :string
    field :value, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(session_type, attrs) do
    session_type
    |> cast(attrs, [:name, :value])
    |> validate_required([:name, :value])
  end
end
