defmodule Boxboxbox.Cars.CarGroup do
  use Ecto.Schema
  import Ecto.Changeset

  schema "car_groups" do
    field :name, :string
    field :value, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(car_group, attrs) do
    car_group
    |> cast(attrs, [:name, :value])
    |> validate_required([:name, :value])
  end
end
