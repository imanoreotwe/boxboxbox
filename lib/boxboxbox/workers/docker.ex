defmodule Boxboxbox.Workers.Docker do
  @moduledoc """
  Genserver for managing docker containers. spawned by docker supervisor.
  """
  use GenServer

  alias Boxboxbox.Docker

  @dockerfile_path "/home/wtromary/dev/boxboxbox/Dockerfile.template"

  # CLIENT API

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @doc "Starts building & deploying server docker container."
  def build_and_run(pid) do
    GenServer.cast(pid, :build_run)
  end

  # SERVER CALLBACKS

  @impl true
  def init(%{server_id: server_id, dir: dir}) do
    {:ok, %{server_id: server_id, dir: dir}}
  end

  # @TODO base image with server already setup w/o configs

  @impl true
    def handle_cast(:build_run, %{server_id: server_id, dir: dir} = state) do
    # build docker image
    build_docker_image(server_id, dir)
    # start docker container
    {:noreply, state}
  end

  @impl true
  def handle_call(:build, _from, %{server_id: server_id, dir: dir} = state) do
    build_docker_image(server_id, dir)
    {:noreply, state}
  end

  @impl true
  def handle_call(:run, %{server_id: server_id}) do

  end

  defp build_docker_image(server_id, dir) do
    IO.puts("hello")
    # gather files & docker image
    File.cp!(@dockerfile_path, Path.join(dir, "Dockerfile"))
    System.cmd("/usr/bin/tar", ["-czf", Path.join(dir, "dockertar.tar.gz"), "-C", dir, "cfg", "Dockerfile"])
    # build docker image
    Docker.build(Path.join(dir, "dockertar.tar.gz"))
    # return docker image id
  end
end
