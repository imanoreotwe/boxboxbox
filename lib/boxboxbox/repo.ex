defmodule Boxboxbox.Repo do
  use Ecto.Repo,
    otp_app: :boxboxbox,
    adapter: Ecto.Adapters.Postgres
end
