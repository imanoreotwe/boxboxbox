defmodule Boxboxbox.DockerSupervisor do
  @moduledoc """
  Dynamic supervisor for controlling docker servers
  """
  use DynamicSupervisor
  alias Boxboxbox.Workers.Docker

  # CLIENT API

  def start_link(args) do
    DynamicSupervisor.start_link(__MODULE__, args, name: __MODULE__)
  end

  def start_child(server_id, dir) do
    DynamicSupervisor.start_child(__MODULE__, {Docker, %{server_id: server_id, dir: dir}})
  end

  # SERVER CALLBACKS

  @impl true
  def init(_args) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
