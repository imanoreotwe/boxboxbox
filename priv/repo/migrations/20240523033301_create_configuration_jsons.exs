defmodule Boxboxbox.Repo.Migrations.CreateConfigurationJsons do
  use Ecto.Migration

  def change do
    create table(:configuration_jsons) do
      add :udpPort, :integer
      add :tcpPort, :integer
      add :registerToLobby, :boolean, default: true, null: false
      add :maxConnections, :integer
      add :lanDiscovery, :boolean, default: false, null: false
      add :publicIP, :string

      timestamps(type: :utc_datetime)
    end
  end
end
