defmodule Boxboxbox.Repo.Migrations.CreateCarGroups do
  use Ecto.Migration

  def change do
    create table(:car_groups) do
      add :name, :string
      add :value, :string

      timestamps(type: :utc_datetime)
    end
  end
end
