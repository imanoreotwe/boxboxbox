defmodule Boxboxbox.Repo.Migrations.CreateServers do
  use Ecto.Migration

  def change do
    create table(:servers) do
      add :owner, references(:users, on_delete: :delete_all)
      add :configuration, references(:configuration_jsons, on_delete: :delete_all)
      add :setting, references(:setting_jsons, on_delete: :delete_all)
      add :event, references(:event_jsons, on_delete: :delete_all)
      add :event_rule, references(:event_rule_jsons, on_delete: :delete_all)
      add :assist_rule, references(:assist_rule_jsons, on_delete: :delete_all)

      timestamps(type: :utc_datetime)
    end

    create index(:servers, [:owner])
  end
end
