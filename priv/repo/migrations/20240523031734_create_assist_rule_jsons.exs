defmodule Boxboxbox.Repo.Migrations.CreateAssistRuleJsons do
  use Ecto.Migration

  def change do
    create table(:assist_rule_jsons) do
      add :stabilityControlLevelMax, :integer
      add :disableAutosteer, :boolean, default: false, null: false
      add :disableAutoLights, :boolean, default: false, null: false
      add :disableAutoWiper, :boolean, default: false, null: false
      add :disableAutoEngineStart, :boolean, default: false, null: false
      add :disableAutoPitLimiter, :boolean, default: false, null: false
      add :disableAutoClutch, :boolean, default: false, null: false
      add :disableAutoGear, :boolean, default: false, null: false
      add :disableIdealLine, :boolean, default: false, null: false

      timestamps(type: :utc_datetime)
    end
  end
end
