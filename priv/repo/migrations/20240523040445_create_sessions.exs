defmodule Boxboxbox.Repo.Migrations.CreateSessions do
  use Ecto.Migration

  def change do
    create table(:sessions) do
      add :hourOfDay, :integer
      add :dayOfWeekend, :integer
      add :timeMultiplier, :integer
      add :sessionDurationMinutes, :integer
      add :sessionType, references(:session_types, on_delete: :nothing)
      add :event, references(:event_jsons, on_delete: :delete_all)

      timestamps(type: :utc_datetime)
    end

    create index(:sessions, [:sessionType])
  end
end
