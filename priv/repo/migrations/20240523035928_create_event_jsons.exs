defmodule Boxboxbox.Repo.Migrations.CreateEventJsons do
  use Ecto.Migration

  def change do
    create table(:event_jsons) do
      add :preRaceWaitingTimeSeconds, :integer
      add :sessionOverTimeSeconds, :integer
      add :ambiantTemp, :integer
      add :cloudLevel, :float
      add :rain, :float
      add :weatherRandomness, :integer
      add :postQualySeconds, :integer
      add :postRaceSeconds, :integer
      add :metaData, :string
      add :track, references(:tracks, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:event_jsons, [:track])
  end
end
