defmodule Boxboxbox.Repo.Migrations.CreateSettingJsons do
  use Ecto.Migration

  def change do
    create table(:setting_jsons) do
      add :serverName, :string
      add :adminPassword, :string
      add :trackMedalsRequirement, :integer
      add :safetyRatingRequirement, :integer
      add :racecraftRatingRequirement, :integer
      add :password, :string
      add :spectatorPassword, :string
      add :maxCarSlots, :integer
      add :dumpLeaderboards, :boolean, default: true, null: false
      add :isRaceLocked, :boolean, default: true, null: true
      add :randomizeTrackWhenEmpty, :boolean, default: false, null: false
      add :centralEntryListPath, :string
      add :allowAutoDQ, :boolean, default: true, null: true
      add :shortFormationLap, :boolean, default: true, null: true
      add :dumpEntryList, :boolean, default: false, null: false
      add :ignorePrematureDisconnects, :boolean, default: false, null: false
      add :carGroup, references(:car_groups, on_delete: :nothing)
      add :formationLapType, references(:formation_lap_types, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:setting_jsons, [:carGroup])
    create index(:setting_jsons, [:formationLapType])
  end
end
