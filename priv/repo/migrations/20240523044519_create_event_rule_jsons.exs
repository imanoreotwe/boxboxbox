defmodule Boxboxbox.Repo.Migrations.CreateEventRuleJsons do
  use Ecto.Migration

  def change do
    create table(:event_rule_jsons) do
      add :pitWindowLengthSec, :integer
      add :driverStintTimeSec, :integer
      add :mandatoryPitstopCount, :integer
      add :maxTotalDrivingTime, :integer
      add :maxDriversCount, :integer
      add :isRefuellingAllowedInRace, :boolean, default: false, null: false
      add :isRefuellingTimeFixed, :boolean, default: false, null: false
      add :isMandatoryPitstopRefuellingRequired, :boolean, default: false, null: false
      add :isMandatoryPitstopTyreChangeRequired, :boolean, default: false, null: false
      add :isMandatoryPitstopSwapDriverRequired, :boolean, default: false, null: false
      add :tyreSetCount, :integer
      add :qualifyStandingType, references(:qualify_types, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:event_rule_jsons, [:qualifyStandingType])
  end
end
