defmodule Boxboxbox.Repo.Migrations.CreateFormationLapTypes do
  use Ecto.Migration

  def change do
    create table(:formation_lap_types) do
      add :name, :string
      add :description, :string
      add :value, :integer

      timestamps(type: :utc_datetime)
    end
  end
end
