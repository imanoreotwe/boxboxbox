defmodule Boxboxbox.Repo.Migrations.CreateQualifyTypes do
  use Ecto.Migration

  def change do
    create table(:qualify_types) do
      add :name, :string
      add :enabled, :boolean, default: false, null: false

      timestamps(type: :utc_datetime)
    end
  end
end
